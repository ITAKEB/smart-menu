{-# LANGUAGE OverloadedStrings          #-}

module Main where

import           Data.CaseInsensitive           ( mk )
import           Data.Yaml                      ( decodeFileEither )
import           Network.HTTP.Types.Header      ( hAuthorization
                                                , hContentType
                                                )
import           Network.HTTP.Types.Method      ( methodDelete
                                                , methodGet
                                                , methodHead
                                                , methodPost
                                                , methodPut
                                                )
import           Network.Wai                    ( Middleware )
import           Network.Wai.Middleware.Cors    ( CorsResourcePolicy(..)
                                                , cors
                                                , simpleCorsResourcePolicy
                                                )
import           Options.Applicative            ( Parser
                                                , ParserInfo
                                                , execParser
                                                , fullDesc
                                                , header
                                                , help
                                                , helper
                                                , info
                                                , long
                                                , metavar
                                                , progDesc
                                                , short
                                                , strOption
                                                )

import           Data.Proxy                     ( Proxy(..) )
import           Network.Wai.Handler.Warp       ( run )
import           Servant.Server                 ( serveWithContext )

import           API.Server                     ( apiServer )
import           API.Types                      ( API )
import           Env.Types                      ( Opts(..) )
import           Env.Utils                      ( mkEnv )
import           Services.User.Auth             ( basicAuthContext )


newtype CmdOpts = CmdOpts
  { configPath :: String
  } deriving (Show)

main :: IO ()
main = do
  cmdOpts    <- execParser parserInfo
  eitherOpts <- decodeFileEither (configPath cmdOpts)
  case eitherOpts of
    Right opts -> do
      env <- mkEnv opts
      putStrLn $ "webapi running on port: " ++ (show . port $ opts)
      let ctx = basicAuthContext env
          app = serveWithContext (Proxy :: Proxy API) ctx (apiServer env)
      run (fromIntegral $ port opts) (corsMiddleware app)
    Left e -> error $ show e

parserInfo :: ParserInfo CmdOpts
parserInfo = info
  (helper <*> optionParser)
  (fullDesc <> header "webapi" <> progDesc "SMART-MENU WEB-API")

optionParser :: Parser CmdOpts
optionParser = CmdOpts <$> strOption
  (long "config-path" <> short 'c' <> metavar "CONFIGPATH" <> help
    "Absolute path to config-file"
  )

corsMiddleware :: Middleware
corsMiddleware = cors (const $ Just corsPolicy)
 where
  hUser = mk "User"
  --
  corsPolicy :: CorsResourcePolicy
  corsPolicy = simpleCorsResourcePolicy
    { corsRequestHeaders = [hAuthorization, hContentType, hUser]
    , corsMethods = [methodGet, methodPost, methodPut, methodDelete, methodHead]
    , corsExposedHeaders = Just [hAuthorization, hContentType, hUser]
    , corsOrigins        = Nothing
    }
