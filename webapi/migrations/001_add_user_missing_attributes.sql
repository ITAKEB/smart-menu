ALTER TABLE xuser
    ADD COLUMN country        VARCHAR(45) NOT NULL,
    ADD COLUMN city           VARCHAR(45) NOT NULL,
    ADD COLUMN is_admin       BOOLEAN NOT NULL DEFAULT false,
    ADD COLUMN birthdate      DATE NOT NULL;
