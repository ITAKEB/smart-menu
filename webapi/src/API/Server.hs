module API.Server
  ( apiServer
  ) where

import           API.Types                      ( API )
import           Codec.Picture.Types            ( DynamicImage )
import           Control.Monad                  ( when )
import           Control.Monad.Except           ( throwError )
import           Control.Monad.Reader           ( asks )
import           Control.Monad.Trans.Reader     ( runReaderT )
import           Data.Image.CRUD                ( selectImageById )
import           Data.Maybe                     ( fromJust
                                                , fromMaybe
                                                , isNothing
                                                )
import           Data.Proxy                     ( Proxy(..) )
import           Data.UUID                      ( UUID )
import           Data.Utils                     ( runSession )
import           Env.Types                      ( APIError
                                                  ( ImageNotFound
                                                  , UnknownError
                                                  )
                                                , APIHandler
                                                , Env(dbConn)
                                                )
import           Env.Utils                      ( actionE
                                                , liftSession
                                                )
import           Numeric.Natural                ( Natural )
import           Servant                        ( (:<|>)(..)
                                                , Handler
                                                , Server
                                                , ServerT
                                                , hoistServerWithContext
                                                )
import qualified Services.Restaurant.Handlers  as RSH
                                                ( handlers )
import qualified Services.Reviews.Handlers     as RVH
                                                ( handlers )
import           Services.User.Auth             ( BasicAuthCtx )
import qualified Services.User.Handlers        as USH
                                                ( handlers )
import           Utils.Image                    ( decodeImg )


handlers :: ServerT API APIHandler
handlers =
  (return ())
    :<|> getImageH
    :<|> USH.handlers
    :<|> RSH.handlers
    :<|> RVH.handlers

getImageH :: UUID -> Maybe Natural -> Maybe Natural -> APIHandler DynamicImage
getImageH imgId w h = actionE $ do
  conn <- asks dbConn
  let w' = fromIntegral $ fromMaybe 500 w
      h' = fromIntegral $ fromMaybe 500 h
  maybeRawImg <- liftSession $ runSession conn $ selectImageById imgId
  when (isNothing maybeRawImg) (throwError ImageNotFound)
  let maybeDecodedImg = decodeImg (fromJust maybeRawImg) (w', h')
  when (isNothing maybeDecodedImg) (throwError UnknownError)
  return $ fromJust maybeDecodedImg

apiServer :: Env -> Server API
apiServer env = hoistServerWithContext (Proxy :: Proxy API)
                                       (Proxy :: Proxy BasicAuthCtx)
                                       natT
                                       handlers
 where
  natT :: APIHandler a -> Handler a
  natT m = runReaderT m env
