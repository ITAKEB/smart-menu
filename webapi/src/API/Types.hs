{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module API.Types
  ( API
  ) where

import           Codec.Picture.Types            ( DynamicImage )
import           Data.UUID                      ( UUID )
import           Numeric.Natural                ( Natural )
import           Servant                        ( (:<|>)
                                                , (:>)
                                                , Capture
                                                , Get
                                                , JSON
                                                , QueryParam
                                                )
import           Servant.JuicyPixels            ( JPEG )


import           Services.Restaurant.API        ( RestaurantAPI )
import           Services.Reviews.API           ( ReviewsAPI )
import           Services.User.API              ( UserAPI )


--
-- Server General API
--
type StatusEndpoint = "status" :> Get '[JSON] ()
type ImageEndpoint = "image"
                  :> Capture "image-id" UUID
                  :> QueryParam "width" Natural
                  :> QueryParam "height" Natural
                  :> Get '[JPEG 100] DynamicImage
type API = StatusEndpoint
      :<|> ImageEndpoint
      :<|> UserAPI
      :<|> RestaurantAPI
      :<|> ReviewsAPI
