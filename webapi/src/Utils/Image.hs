{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}

module Utils.Image
  ( decodeImg
  ) where

import           Codec.Picture                  ( DynamicImage(ImageRGB8)
                                                , convertRGB8
                                                , decodeJpeg
                                                )
import           Codec.Picture.Extra            ( scaleBilinear )
import qualified Data.ByteString               as SB
                                                ( ByteString )


decodeImg :: SB.ByteString -> (Int, Int) -> Maybe DynamicImage
decodeImg bs (w, h) = case decodeJpeg bs of
  Left _ -> Nothing
  Right image ->
    let imgRGB8 = convertRGB8 image
    in  Just $ ImageRGB8 (scaleBilinear w h imgRGB8)
