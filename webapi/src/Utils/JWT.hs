{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}

module Utils.JWT
  ( EncodedJWT(..)
  , EncodedJWK
  , decodeJWK
  , genJWPair
  , getUserIdFromSub
  , verifyJWT
  ) where

import           Control.Lens                   ( (&)
                                                , (.~)
                                                , re
                                                , set
                                                , view
                                                )
import           Control.Monad                  ( join )
import           Control.Monad.Except           ( ExceptT
                                                , runExceptT
                                                )
import           Crypto.JOSE.Compact            ( decodeCompact )
import           Crypto.JWT                     ( Audience(..)
                                                , ClaimsSet
                                                , Digest
                                                , JWK
                                                , JWTError
                                                , KeyMaterialGenParam
                                                  ( OctGenParam
                                                  )
                                                , NumericDate(..)
                                                , SHA256
                                                , SignedJWT
                                                , base64url
                                                , bestJWSAlg
                                                , claimAud
                                                , claimExp
                                                , claimIat
                                                , claimIss
                                                , claimJti
                                                , claimSub
                                                , defaultJWTValidationSettings
                                                , digest
                                                , emptyClaimsSet
                                                , encodeCompact
                                                , genJWK
                                                , jwkKid
                                                , jwtValidationSettingsIssuerPredicate
                                                , newJWSHeader
                                                , signClaims
                                                , string
                                                , thumbprint
                                                , verifyClaims
                                                )
import           Data.Aeson                     ( FromJSON
                                                , ToJSON(..)
                                                , eitherDecode
                                                , encode
                                                )
import qualified Data.ByteString.Base64.Lazy   as B64
import qualified Data.ByteString.Lazy          as LBS
import           Data.ByteString.Lazy           ( ByteString
                                                , toStrict
                                                )
import           Data.String                    ( fromString )
import qualified Data.Text                     as T
import           Data.Text                      ( Text )
import           Data.Text.Encoding             ( decodeUtf8
                                                , encodeUtf8
                                                )
import           Data.Text.Strict.Lens          ( utf8 )
import           Data.Time.Clock                ( NominalDiffTime
                                                , UTCTime
                                                , addUTCTime
                                                )
import           Data.UUID                      ( UUID
                                                , fromText
                                                , toText
                                                )
import           Data.UUID.V4                   ( nextRandom )
import           GHC.Generics
import           Servant                        ( FromHttpApiData
                                                , ToHttpApiData
                                                , parseUrlPiece
                                                )


type IssuedAt = UTCTime
type EncodedJWK = ByteString

newtype EncodedJWT = EncodedJWT { jwt :: Text } deriving ( Eq , Show , ToHttpApiData, Generic )

-- | Requests with JWT token in Headers are sent with Bearer scheme.
instance FromHttpApiData EncodedJWT where
  parseUrlPiece token = maybe (Left "Invalid authentication scheme")
                              (Right . EncodedJWT)
                              (stripBearerScheme token)

instance FromJSON EncodedJWT
instance ToJSON EncodedJWT

genJWPair
  :: Text
  -> UUID
  -> IssuedAt
  -> NominalDiffTime
  -> IO (Either JWTError (EncodedJWK, EncodedJWT))
genJWPair aud uid issuedAt expPeriod = do
  let aud' = T.unpack aud
  claims <- mkClaims aud' uid issuedAt expPeriod
  key    <- genJWKey
  eJwt   <- signJWT key claims
  return $ either Left (\jwt' -> Right (encodeJWK key, encodeJWT jwt')) eJwt

verifyJWT :: JWK -> EncodedJWT -> IO (Either JWTError ClaimsSet)
verifyJWT key jwt' = runExceptT $ do
  signedJwt <- decodeSignedJWT jwt'
  verifyClaims conf key signedJwt
 where
  conf =
    defaultJWTValidationSettings (const True)
      &  jwtValidationSettingsIssuerPredicate
      .~ (== "smartmenu")
  --
  decodeSignedJWT :: EncodedJWT -> ExceptT JWTError IO SignedJWT
  decodeSignedJWT = decodeCompact . LBS.fromStrict . encodeUtf8 . jwt

decodeJWK :: LBS.ByteString -> Either String JWK
decodeJWK bs = do
  token <- B64.decode bs
  eitherDecode token

getUserIdFromSub :: ClaimsSet -> Maybe UUID
getUserIdFromSub cs =
  let sub = view string <$> (view claimSub cs) in join $ fmap fromText sub

--
-- Helpers
--
genJWKey :: IO JWK
genJWKey = do
  key <- genJWK (OctGenParam 72)
  let h   = view thumbprint key :: Digest SHA256
      kid = view (re (base64url . digest) . utf8) h
  pure $ set jwkKid (Just kid) key

signJWT :: JWK -> ClaimsSet -> IO (Either JWTError SignedJWT)
signJWT key claims = runExceptT $ do
  algo <- bestJWSAlg key
  signClaims key (newJWSHeader ((), algo)) claims

encodeJWT :: SignedJWT -> EncodedJWT
encodeJWT = EncodedJWT . decodeUtf8 . toStrict . encodeCompact

encodeJWK :: JWK -> ByteString
encodeJWK = B64.encode . encode . toJSON

stripBearerScheme :: Text -> Maybe Text
stripBearerScheme text = do
  suffix <- T.stripPrefix "Bearer" text
  return (T.stripEnd . T.stripStart $ suffix)

mkClaims :: String -> UUID -> IssuedAt -> NominalDiffTime -> IO ClaimsSet
mkClaims aud uid issuedAt expPeriod = do
  jwtId <- toText <$> nextRandom
  return
    $  emptyClaimsSet
    &  claimSub
    .~ Just (fromString $ T.unpack $ toText uid)
    &  claimIss
    .~ Just "smartmenu"
    &  claimExp
    .~ Just (NumericDate $ addUTCTime expPeriod issuedAt)
    &  claimAud
    .~ Just (Audience [fromString aud]) -- ^ The audience is the only user who was authenticated.
    &  claimIat
    .~ Just (NumericDate issuedAt)
    &  claimJti
    .~ Just jwtId -- ^ Unique tokenId which adds more safety.
