{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Env.Types
  ( Opts(..)
  , Env(..)
  , APIError(..)
  , DBSettings(..)
  , APIHandlerE(..)
  , APIHandler
  ) where

import           Control.Exception.Base         ( Exception )
import           Control.Monad.Catch            ( MonadCatch
                                                , MonadThrow
                                                )
import           Control.Monad.Except           ( ExceptT
                                                , MonadError(..)
                                                )
import           Control.Monad.IO.Class         ( MonadIO )
import           Control.Monad.Reader           ( MonadReader )
import           Control.Monad.Trans.Reader     ( ReaderT )
import           Data.Aeson                     ( FromJSON(..)
                                                , withText
                                                )
import qualified Data.Text                     as T
import           Data.Word                      ( Word16 )
import           GHC.Generics
import           Hasql.Connection               ( Connection )
import           Hasql.Session                  ( QueryError )
import           Servant                        ( Handler )
import qualified System.Logger                 as TL
import           Text.Read                      ( readMaybe )


data Opts = Opts
  { psqlSettings :: DBSettings
  , logLevel     :: TL.Level
  , port         :: Word16
  }
  deriving (Show, Generic)

instance FromJSON Opts

data DBSettings = DBSettings
  { pqHost     :: String
  , pqPort     :: Word16
  , pqUser     :: String
  , pqPassword :: String
  , pqDatabase :: String
  }
  deriving (Show, Generic)

instance FromJSON DBSettings

instance FromJSON TL.Level where
  parseJSON = withText "Level" $ \lvl -> case readMaybe . T.unpack $ lvl of
    Just lv -> pure lv
    Nothing ->
      fail "Invalid log Level. Valid values: Trace, Debug, Info, Error"

data Env = Env
  { dbConn    :: Connection
  , appLogger :: TL.Logger
  }

data APIError = UnknownError
              | DatabaseError QueryError
              | ExistentEmail
              | InternalError
              | InvalidAuthToken
              | InvalidImgFormat
              | MenuNotFound
              | NoJWKStored
              | NotAllowedToReview
              | NotAnAdmin
              | RestaurantNotFound
              | UserNotFound
              | ImageTooLarge
              | ImageNotFound
    deriving Show

instance Exception APIError

type APIHandler = ReaderT Env Handler

newtype APIHandlerE a = APIHandlerE
  { unAPIHandlerE :: ExceptT APIError APIHandler a
  } deriving ( Functor
             , Applicative
             , Monad
             , MonadCatch
             , MonadIO
             , MonadReader Env
             , MonadThrow
             , MonadError APIError
             )
