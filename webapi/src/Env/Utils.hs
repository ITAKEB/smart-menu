{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE OverloadedStrings          #-}

module Env.Utils
  ( releaseConnection
  , actionE
  , fromLeft'
  , fromRight'
  , liftSession
  , logErrText
  , mkEnv
  ) where

import           Control.Monad                  ( when )
import           Control.Monad.Except           ( MonadError(..)
                                                , runExceptT
                                                )
import           Control.Monad.IO.Class         ( MonadIO
                                                , liftIO
                                                )
import           Control.Monad.Reader           ( MonadReader
                                                , asks
                                                )
import qualified Data.ByteString.Char8         as C8
import qualified Data.ByteString.Lazy          as LBS
import           Data.Either                    ( fromLeft
                                                , fromRight
                                                , isLeft
                                                )
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import qualified Data.Text.Encoding            as T
                                                ( decodeUtf8
                                                , encodeUtf8
                                                )
import           Hasql.Connection               ( Connection
                                                , Settings
                                                , acquire
                                                , release
                                                , settings
                                                )
import           Hasql.Session                  ( QueryError(..) )
import           Servant                        ( ServerError(..) )
import           Servant.Server                 ( err400
                                                , err401
                                                , err404
                                                , err500
                                                )
import qualified System.Logger                 as TL

import           Env.Types                      ( APIError
                                                  ( DatabaseError
                                                  , ExistentEmail
                                                  , ImageNotFound
                                                  , ImageTooLarge
                                                  , InternalError
                                                  , InvalidAuthToken
                                                  , InvalidImgFormat
                                                  , MenuNotFound
                                                  , NoJWKStored
                                                  , NotAllowedToReview
                                                  , NotAnAdmin
                                                  , RestaurantNotFound
                                                  , UnknownError
                                                  , UserNotFound
                                                  )
                                                , APIHandler
                                                , APIHandlerE(..)
                                                , DBSettings(..)
                                                , Env(..)
                                                , Opts(..)
                                                )


mkEnv :: Opts -> IO Env
mkEnv opts = do
  conn   <- mkConnection $ psqlSettings opts
  logger <- mkLogger $ logLevel opts
  return $ Env conn logger
 where
  mkLogger :: TL.Level -> IO TL.Logger
  mkLogger lvl =
    TL.new . TL.setReadEnvironment False . TL.setLogLevel lvl $ TL.defSettings

releaseConnection :: Connection -> IO ()
releaseConnection = release

liftSession :: IO (Either QueryError a) -> APIHandlerE a
liftSession action = do
  result <- liftIO action
  dbActionGuard result
 where
  dbActionGuard :: Either QueryError a -> APIHandlerE a
  dbActionGuard eitherResult = do
    when (isLeft eitherResult)
         (throwError $ DatabaseError $ fromLeft' eitherResult)
    return $ fromRight' eitherResult

fromLeft' :: Either a b -> a
fromLeft' = fromLeft (error "Attempt to deconstruct Right as Left")

fromRight' :: Either a b -> b
fromRight' = fromRight (error "Attempt to deconstruct Left as Right")

actionE :: APIHandlerE a -> APIHandler a
actionE m = runExceptT (unAPIHandlerE m) >>= either failWith return
 where
  toLazyBs = LBS.fromStrict . T.encodeUtf8 . T.pack . show
  --
  throw e code = throwError code { errBody = toLazyBs e }
  --
  failWith :: APIError -> APIHandler a
  failWith e@UnknownError       = throw e err500
  failWith e@InternalError      = throw e err500
  failWith e@UserNotFound       = throw e err404
  failWith e@RestaurantNotFound = throw e err404
  failWith e@MenuNotFound       = throw e err404
  failWith e@NoJWKStored        = throw e err401
  failWith e@ExistentEmail      = throw e err400
  failWith e@NotAnAdmin         = throw e err401
  failWith e@InvalidAuthToken   = throw e err400
  failWith e@NotAllowedToReview = throw e err400
  failWith e@InvalidImgFormat   = throw e err400
  failWith e@ImageTooLarge      = throw e err400
  failWith e@ImageNotFound      = throw e err404
  failWith (DatabaseError qe)   = do
    logErrText $ queryErrToText qe
    throw InternalError err500

logErrText :: (MonadIO m, MonadReader Env m) => Text -> m ()
logErrText t = asks appLogger >>= (\logger -> TL.err logger (TL.msg t))

--
-- Helpers
--
mkConnection :: DBSettings -> IO Connection
mkConnection ss = do
  eitherConn <- acquire connString
  case eitherConn of
    Left  Nothing    -> error "Unknown connection error."
    Left  (Just err) -> error $ C8.unpack err
    Right conn       -> return conn
 where
  connString :: Settings
  connString = settings (C8.pack $ pqHost ss)
                        (pqPort ss)
                        (C8.pack $ pqUser ss)
                        (C8.pack $ pqPassword ss)
                        (C8.pack $ pqDatabase ss)

queryErrToText :: QueryError -> Text
queryErrToText (QueryError msg params _) =
  "Query: " <> T.decodeUtf8 msg <> " <> Params: " <> T.intercalate "," params
