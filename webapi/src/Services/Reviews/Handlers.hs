{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE FlexibleContexts           #-}

module Services.Reviews.Handlers
  ( handlers
  ) where

import           Control.Monad                  ( when )
import           Control.Monad.Except           ( throwError )
import           Control.Monad.IO.Class         ( liftIO )
import           Control.Monad.Reader           ( asks )
import           Data.Int                       ( Int32 )
import           Data.Maybe                     ( fromMaybe )
import           Data.Time.Clock                ( diffUTCTime
                                                , getCurrentTime
                                                , nominalDay
                                                )
import           Data.UUID                      ( UUID )
import           Data.UUID.V4                   ( nextRandom )
import           Data.Vector                    ( Vector )
import           Numeric.Natural                ( Natural )
import           Servant                        ( (:<|>)(..)
                                                , ServerT
                                                )


import           Data.Reviews.CRUD              ( insertReview
                                                , selectAverageRating
                                                , selectItemReviews
                                                , selectReviewNumLikes
                                                , selectUserLastReviewTime
                                                , updateLike
                                                )
import           Data.Utils                     ( runSession )
import           Env.Types                      ( APIError(NotAllowedToReview)
                                                , APIHandler
                                                , Env(dbConn)
                                                )
import           Env.Utils                      ( actionE
                                                , liftSession
                                                )
import           Services.Reviews.API           ( ReviewsAPI )
import           Services.Reviews.Types         ( AverageRating(..)
                                                , Category(..)
                                                , Review
                                                )

import qualified Services.Reviews.Types        as PRR
                                                ( PostReviewReq(..) )


import           Services.User.Auth             ( withAuth )
import           Utils.JWT                      ( EncodedJWT )


handlers :: ServerT ReviewsAPI APIHandler
handlers =
  postReviewH
    :<|> getAverageRatingsH
    :<|> getItemLastReviewsH
    :<|> postReviewLikeH
    :<|> getReviewNumLikes

--
-- Handler definitions
--
postReviewH :: EncodedJWT -> UUID -> UUID -> PRR.PostReviewReq -> APIHandler ()
postReviewH jwt' uId iId rv = withAuth uId jwt' $ \_ -> do
  rId       <- liftIO nextRandom
  conn      <- asks dbConn
  maybeTime <- liftSession $ runSession conn $ selectUserLastReviewTime
    uId
    iId
    (PRR.category rv)
  reviewGuard maybeTime
  liftSession $ runSession conn $ insertReview rId
                                               uId
                                               iId
                                               (PRR.category rv)
                                               (PRR.score rv)
                                               (PRR.content rv)
 where
  reviewGuard maybeTime = case maybeTime of
    Nothing -> return ()
    Just t  -> do
      now <- liftIO getCurrentTime
      let diff = diffUTCTime now t
      when (diff < nominalDay) (throwError NotAllowedToReview)

getAverageRatingsH
  :: UUID
  -> APIHandler (AverageRating, AverageRating, AverageRating, AverageRating)
getAverageRatingsH iId = actionE $ do
  let dbActions = mapM getAvg [Quality, PriceFairness, Ecology, Healthiness]
  conn <- asks dbConn
  as   <- liftSession $ runSession conn dbActions
  return (as !! 0, as !! 1, as !! 2, as !! 3)
 where
  getAvg cat = do
    maybeAvg <- selectAverageRating iId cat
    let toRating = AverageRating cat
    case maybeAvg of
      Nothing  -> return $ toRating 0
      Just avg -> return $ toRating avg

getItemLastReviewsH
  :: UUID
  -> Category
  -> Maybe Int32
  -> Maybe Int32
  -> APIHandler (Vector Review)
getItemLastReviewsH iId cat maybeOffset maybeTo = actionE $ do
  conn <- asks dbConn
  liftSession $ runSession conn $ selectItemReviews iId cat (offset, to)
 where
  offset = fromMaybe 0 maybeOffset
  --
  to     = fromMaybe 10 maybeTo

postReviewLikeH :: EncodedJWT -> UUID -> UUID -> APIHandler Bool
postReviewLikeH jwt' uId rId = withAuth uId jwt' $ \_ -> do
  conn <- asks dbConn
  lId  <- liftIO nextRandom
  liftSession $ runSession conn $ updateLike lId uId rId

getReviewNumLikes :: UUID -> APIHandler Natural
getReviewNumLikes rId = actionE $ do
  conn <- asks dbConn
  liftSession $ runSession conn $ selectReviewNumLikes rId
