{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings          #-}

module Services.Reviews.Types
  ( Category(..)
  , AverageRating(..)
  , PostReviewReq(..)
  , Review(..)
  ) where

import           Data.Aeson                     ( FromJSON(..)
                                                , ToJSON(..)
                                                )
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import           Data.UUID                      ( UUID )
import           GHC.Generics
import           Numeric.Natural                ( Natural )
import           Servant.API                    ( FromHttpApiData(..) )


data Category = Quality
              | Healthiness
              | PriceFairness
              | Ecology
              deriving (Eq, Ord, Enum, Show, Generic, Read)

instance FromHttpApiData Category where
  parseQueryParam txt = case T.unpack txt of
    "Quality"       -> Right Quality
    "Healthiness"   -> Right Healthiness
    "PriceFairness" -> Right PriceFairness
    "Ecology"       -> Right Ecology
    _               -> Left "Could not parse query param for `Category`"

instance FromJSON Category
instance ToJSON Category

data PostReviewReq = PostReviewReq
  { category :: Category
  , score    :: Float
  , content  :: Maybe Text
  }
  deriving (Eq, Show, Generic)

instance FromJSON PostReviewReq

data Review = Review
  { idx      :: UUID
  , category :: Category
  , userId   :: UUID
  , itemId   :: UUID
  , score    :: Float
  , userName :: Text
  , likes    :: Natural
  , content  :: Maybe Text
  }
  deriving (Eq, Show, Generic)

instance ToJSON Review

data AverageRating = AverageRating
  { category :: Category
  , average  :: Float
  }
  deriving (Eq, Show, Generic)

instance ToJSON AverageRating
