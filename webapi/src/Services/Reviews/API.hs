{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module Services.Reviews.API where

import           Data.Int                       ( Int32 )
import           Data.UUID                      ( UUID )
import           Data.Vector                    ( Vector )
import           Numeric.Natural                ( Natural )
import           Servant                        ( (:<|>)
                                                , (:>)
                                                , Capture
                                                , Get
                                                , Header'
                                                , JSON
                                                , Post
                                                , QueryParam
                                                , QueryParam'
                                                , ReqBody
                                                , Required
                                                )

import           Services.Reviews.Types         ( AverageRating
                                                , Category
                                                , PostReviewReq
                                                , Review
                                                )
import           Utils.JWT                      ( EncodedJWT )


type Auth = Header' '[Required] "Authorization" EncodedJWT
type HUser = Header' '[Required] "User" UUID
type RequiredQueryParam = QueryParam' '[Required]

type ReviewsAPI = Auth :> HUser
                       :> "restaurants" :> "menus"
                       :> "items" :> Capture "item-id" UUID
                       :> "reviews"
                       :> ReqBody '[JSON] PostReviewReq
                       :> Post '[JSON] ()
                 :<|>     "restaurants" :> "menus"
                       :> "items" :> Capture "item-id" UUID
                       :> "ratings" :> "average"
                       :> Get '[JSON] (AverageRating, AverageRating, AverageRating, AverageRating)
                 :<|>     "restaurants" :> "menus"
                       :> "items" :> Capture "item-id" UUID
                       :> RequiredQueryParam "category" Category
                       :> QueryParam "offset" Int32
                       :> QueryParam "to" Int32
                       :> "reviews" :> Get '[JSON] (Vector Review)
                 :<|>  Auth :> HUser
                       :> "restaurants" :> "menus"
                       :> "items" :> "reviews"
                       :> "likes" :> Capture "review-id" UUID
                       :> Post '[JSON] Bool
                 :<|>     "restaurants" :> "menus"
                       :> "items" :> "reviews"
                       :> "likes" :> Capture "review-id" UUID
                       :> Get '[JSON] Natural
