{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Services.User.Types
  ( HashedPassword(..)
  , CreateUserReq(..)
  , HistoryReview(..)
  , Recommendation(..)
  , User(..)
  , UserScore(..)
  , UserWithJWT(..)
  ) where

import           Data.Aeson                     ( FromJSON(..)
                                                , ToJSON(..)
                                                )
import           Data.ByteString                ( ByteString )
import           Data.Scientific                ( Scientific )
import           Data.Text                      ( Text )
import           Data.Time.Calendar             ( Day )
import           Data.UUID                      ( UUID )
import           GHC.Generics
import           Numeric.Natural                ( Natural )

import           Services.Reviews.Types         ( Category )
import           Utils.JWT                      ( EncodedJWT )

data HashedPassword = HashedPassword
  { getHash :: ByteString
  }
  deriving (Eq, Show)

data CreateUserReq = CreateUserReq
  { email     :: Text
  , name      :: Text
  , password  :: Text
  , country   :: Text
  , city      :: Text
  , birthdate :: Day
  , isAdmin   :: Bool
  }
  deriving (Eq, Show, Generic)

instance FromJSON CreateUserReq

data User = User
  { idx       :: UUID
  , email     :: Text
  , name      :: Text
  , country   :: Text
  , city      :: Text
  , birthdate :: Day
  , isAdmin   :: Bool
  }
  deriving (Eq, Show, Generic)

instance ToJSON User

data UserWithJWT = UserWithJWT
  { user  :: User
  , token :: EncodedJWT
  }
  deriving (Eq, Show, Generic)

instance ToJSON UserWithJWT

data HistoryReview = HistoryReview
  { category  :: Category
  , score     :: Float
  , likes     :: Natural
  , content   :: Maybe Text
  , itemTitle :: Text
  , imageId   :: Maybe UUID
  , price     :: Scientific
  }
  deriving (Eq, Show, Generic)

instance ToJSON HistoryReview

data Recommendation = Recommendation
  { itemTitle   :: Text
  , imageId     :: Maybe UUID
  , price       :: Scientific
  , score       :: Float
  , resturantId :: UUID
  , mealTypeId  :: UUID
  }
  deriving (Eq, Show, Generic)

instance ToJSON Recommendation

newtype UserScore = UserScore { score :: Float }
  deriving (Eq, Show, Generic)

instance ToJSON UserScore
