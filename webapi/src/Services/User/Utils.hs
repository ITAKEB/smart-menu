module Services.User.Utils
  ( hashPassword
  , validatePassword
  ) where

import qualified Crypto.KDF.BCrypt             as C
import           Data.ByteString                ( ByteString )
import           Data.Text                      ( Text )
import           Data.Text.Encoding             ( encodeUtf8 )

import           Services.User.Types            ( HashedPassword(..) )


hashPassword :: Text -> IO HashedPassword
hashPassword = fmap HashedPassword . C.hashPassword 4 . encodeUtf8

validatePassword :: ByteString -> HashedPassword -> Bool
validatePassword rawPassword (HashedPassword hash) =
  C.validatePassword rawPassword hash
