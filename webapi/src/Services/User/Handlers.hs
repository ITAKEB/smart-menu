{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE FlexibleContexts           #-}

module Services.User.Handlers
  ( handlers
  ) where

import           Control.Monad                  ( when )
import           Control.Monad.Except           ( throwError )
import           Control.Monad.IO.Class         ( liftIO )
import           Control.Monad.Reader           ( asks )
import           Data.Either                    ( isLeft )
import           Data.Int                       ( Int32 )
import           Data.Maybe                     ( fromMaybe
                                                , isJust
                                                )
import           Data.Time.Clock                ( getCurrentTime )
import           Data.UUID                      ( UUID )
import           Data.UUID.V4                   ( nextRandom )
import           Data.Vector                    ( Vector )
import           Servant                        ( (:<|>)(..)
                                                , ServerT
                                                )


import           Data.User.CRUD                 ( insertUser
                                                , selectAverageUserScore
                                                , selectItemRecommendations
                                                , selectUserByEmail
                                                , selectUserLastReviews
                                                , updateUserJWK
                                                )
import           Data.Utils                     ( runSession )
import           Env.Types                      ( APIError
                                                  ( ExistentEmail
                                                  , InternalError
                                                  )
                                                , APIHandler
                                                , Env(dbConn)
                                                )
import           Env.Utils                      ( actionE
                                                , fromRight'
                                                , liftSession
                                                )
import           Services.Reviews.Types         ( Category )
import           Services.User.API              ( UserAPI )
import           Services.User.Auth             ( withAuth )
import qualified Services.User.Types           as USC
                                                ( UserScore(..) )
import qualified Services.User.Types           as CUR
                                                ( CreateUserReq(..) )
import qualified Services.User.Types           as USR
                                                ( User(..) )
import           Services.User.Types            ( HistoryReview
                                                , Recommendation
                                                )
import qualified Services.User.Types           as UST
                                                ( UserWithJWT(..) )
import           Services.User.Utils            ( hashPassword )
import           Utils.JWT                      ( EncodedJWT
                                                , genJWPair
                                                )



handlers :: ServerT UserAPI APIHandler
handlers =
  postUserH
    :<|> authenticateUser
    :<|> revokeTokenH
    :<|> getUserScoreByCategory
    :<|> getUserLastReviewsH
    :<|> getUserRecommendationsH

--
-- Handler definitions
--
authenticateUser :: UST.UserWithJWT -> APIHandler UST.UserWithJWT
authenticateUser = return

postUserH :: CUR.CreateUserReq -> APIHandler EncodedJWT
postUserH reqBody = actionE $ do
  conn      <- asks dbConn
  maybeUser <- liftSession $ runSession conn $ selectUserByEmail $ CUR.email
    reqBody
  when (isJust maybeUser) (throwError ExistentEmail)
  uid        <- liftIO nextRandom
  hash       <- liftIO $ hashPassword $ CUR.password reqBody
  (jwk, jwt) <- genSession uid (CUR.email reqBody)
  let user = USR.User { USR.idx       = uid
                      , USR.email     = CUR.email reqBody
                      , USR.name      = CUR.name reqBody
                      , USR.city      = CUR.city reqBody
                      , USR.country   = CUR.country reqBody
                      , USR.isAdmin   = CUR.isAdmin reqBody
                      , USR.birthdate = CUR.birthdate reqBody
                      }
      dbActions = insertUser user hash >> updateUserJWK (uid, Just jwk)
  liftSession $ runSession conn dbActions
  return jwt
 where
  genSession uid email = do
    let expirationPeriodInSecs = 86400 * 100 -- 100 Days
    issuedAt   <- liftIO getCurrentTime
    eitherPair <- liftIO $ genJWPair email uid issuedAt expirationPeriodInSecs
    when (isLeft eitherPair) (throwError InternalError)
    return $ fromRight' eitherPair

revokeTokenH :: EncodedJWT -> UUID -> APIHandler ()
revokeTokenH jwt' idx = withAuth idx jwt' $ \_ -> do
  conn <- asks dbConn
  liftSession $ runSession conn $ updateUserJWK (idx, Nothing)

getUserScoreByCategory
  :: EncodedJWT -> UUID -> Category -> APIHandler USC.UserScore
getUserScoreByCategory jwt' uId cat = withAuth uId jwt' $ \_ -> do
  conn <- asks dbConn
  s    <- liftSession $ runSession conn $ selectAverageUserScore uId cat
  return $ USC.UserScore (fromMaybe 0 s)

getUserLastReviewsH
  :: EncodedJWT
  -> UUID
  -> Category
  -> Maybe Int32
  -> Maybe Int32
  -> APIHandler (Vector HistoryReview)
getUserLastReviewsH jwt' uId cat maybeOffset maybeTo =
  withAuth uId jwt' $ \_ -> do
    conn <- asks dbConn
    liftSession $ runSession conn $ selectUserLastReviews uId cat (offset, to)
 where
  offset = fromMaybe 0 maybeOffset
  --
  to     = fromMaybe 10 maybeTo

getUserRecommendationsH
  :: EncodedJWT
  -> UUID
  -> Category
  -> Maybe Int32
  -> Maybe Int32
  -> APIHandler (Vector Recommendation)
getUserRecommendationsH jwt' uId cat maybeOffset maybeTo =
  withAuth uId jwt' $ \_ -> do
    conn <- asks dbConn
    liftSession $ runSession conn getRecommendations
 where
  getRecommendations = do
    s <- selectAverageUserScore uId cat
    selectItemRecommendations (improveScore s) cat (offset, to)
  --
  improveScore Nothing = 4
  improveScore (Just s) =
    let s' = fromIntegral $ (ceiling s :: Int)
    in  if s' == s then s' + 1 else s'
  --
  offset = fromMaybe 0 maybeOffset
  --
  to     = fromMaybe 10 maybeTo
