{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Services.User.Auth
  ( BasicAuthCtx
  , basicAuthContext
  , validateJWT
  , withAuth
  , withAdmin
  ) where

import           Control.Monad                  ( when )
import           Control.Monad.Except           ( throwError )
import           Control.Monad.IO.Class         ( liftIO )
import           Control.Monad.Reader           ( asks )
import           Crypto.JWT                     ( ClaimsSet )
import qualified Data.ByteString.Lazy          as LBS
import           Data.Either                    ( isLeft )
import           Data.Maybe                     ( fromJust
                                                , isNothing
                                                )
import           Data.Text.Encoding             ( decodeUtf8 )
import           Data.Time.Clock                ( getCurrentTime )
import           Data.UUID                      ( UUID )
import           Env.Types                      ( APIError
                                                  ( InternalError
                                                  , InvalidAuthToken
                                                  , NoJWKStored
                                                  , NotAnAdmin
                                                  )
                                                )
import           Servant                        ( Context((:.), EmptyContext) )
import           Servant.API                    ( BasicAuthData(..) )
import           Servant.Server                 ( BasicAuthCheck(..)
                                                , BasicAuthResult
                                                  ( Authorized
                                                  , BadPassword
                                                  , NoSuchUser
                                                  )
                                                )

import           Data.User.CRUD                 ( isUserAdmin
                                                , selectUserJWKById
                                                , selectUserWithPasswordByEmail
                                                , updateUserJWK
                                                )
import           Data.Utils                     ( runSession )
import           Env.Types                      ( APIHandler
                                                , APIHandlerE
                                                , Env(..)
                                                )
import           Env.Utils                      ( actionE
                                                , fromRight'
                                                , liftSession
                                                )
import qualified Services.User.Types           as USR
                                                ( User(..) )
import qualified Services.User.Types           as UST
                                                ( UserWithJWT(..) )
import           Services.User.Utils            ( validatePassword )
import           Utils.JWT                      ( EncodedJWT
                                                , decodeJWK
                                                , genJWPair
                                                , verifyJWT
                                                )


type BasicAuthCtx = '[BasicAuthCheck UST.UserWithJWT]

basicAuthContext :: Env -> Context BasicAuthCtx
basicAuthContext = (:. EmptyContext) . authCheck

authCheck :: Env -> BasicAuthCheck UST.UserWithJWT
authCheck env = BasicAuthCheck check
 where
  check :: BasicAuthData -> IO (BasicAuthResult UST.UserWithJWT)
  check (BasicAuthData email rawPass) = do
    let emailTxt = decodeUtf8 email
    eitherUser <- runSession (dbConn env)
      $ selectUserWithPasswordByEmail emailTxt
    case eitherUser of
      Left _ -> error "Error on selectUserWithPasswordByEmail"
      Right Nothing -> return NoSuchUser
      Right (Just (user, passwordHash)) -> do
        if validatePassword rawPass passwordHash
          then do
            let expirationPeriodInSecs = 86400 * 100 -- 100 Days
            issuedAt <- getCurrentTime
            eJwtPair <- genJWPair emailTxt
                                  (USR.idx user)
                                  issuedAt
                                  expirationPeriodInSecs
            when (isLeft eJwtPair) (error "Error on genJWPair")
            let (jwk, jwt) = fromRight' eJwtPair
            result <- runSession (dbConn env)
              $ updateUserJWK (USR.idx user, Just jwk)
            when (isLeft result) (error "Error on updateUserJWK")
            return $ Authorized
              (UST.UserWithJWT { UST.user = user, UST.token = jwt })
          else return BadPassword

withAuth :: UUID -> EncodedJWT -> (ClaimsSet -> APIHandlerE a) -> APIHandler a
withAuth uid' jwt' action = actionE $ validateJWT uid' jwt' >>= action

withAdmin :: UUID -> EncodedJWT -> (ClaimsSet -> APIHandlerE a) -> APIHandler a
withAdmin uid' jwt' action = actionE $ do
  claims   <- validateJWT uid' jwt'
  conn     <- asks dbConn
  isAdmin' <- liftSession $ runSession conn $ isUserAdmin uid'
  if isAdmin' then action claims else throwError NotAnAdmin

validateJWT :: UUID -> EncodedJWT -> APIHandlerE ClaimsSet
validateJWT uid' jwt' = do
  conn  <- asks dbConn
  jwkBs <- liftSession $ runSession conn $ selectUserJWKById uid'
  when (isNothing jwkBs) (throwError NoJWKStored)
  let eitherJwk = decodeJWK $ LBS.fromStrict $ fromJust jwkBs
  when (isLeft eitherJwk) (throwError InternalError)
  let jwk = fromRight' eitherJwk
  eitherClaims <- liftIO $ verifyJWT jwk jwt'
  when (isLeft eitherClaims) (throwError InvalidAuthToken)
  return $ fromRight' eitherClaims
