{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module Services.User.API where

import           Data.Int                       ( Int32 )
import           Data.UUID                      ( UUID )
import           Data.Vector                    ( Vector )
import           Servant                        ( (:<|>)
                                                , (:>)
                                                , BasicAuth
                                                , Capture
                                                , Delete
                                                , Get
                                                , Header'
                                                , JSON
                                                , Post
                                                , Put
                                                , QueryParam
                                                , QueryParam'
                                                , ReqBody
                                                , Required
                                                )

import           Services.Reviews.Types         ( Category )
import           Services.User.Types            ( HistoryReview
                                                , UserScore
                                                )
import           Services.User.Types            ( CreateUserReq
                                                , Recommendation
                                                , UserWithJWT
                                                )
import           Utils.JWT                      ( EncodedJWT )


type Auth = Header' '[Required] "Authorization" EncodedJWT
type HUser = Header' '[Required] "User" UUID
type RequiredQueryParam = QueryParam' '[Required]

type UserAPI =   "users" :> ReqBody '[JSON] CreateUserReq :> Post '[JSON] EncodedJWT
            :<|> "users" :> "token" :> BasicAuth "credentials" UserWithJWT :> Put '[JSON] UserWithJWT
            :<|> Auth :> "users" :> Capture "user-id" UUID :> "token" :> "revoke" :> Delete '[JSON] ()
            :<|> Auth :> HUser
                      :> "users"
                      :> "score"
                      :> RequiredQueryParam "category" Category
                      :> Get '[JSON] UserScore
            :<|> Auth :> HUser
                      :> "users"
                      :> "reviews"
                      :> RequiredQueryParam "category" Category
                      :> QueryParam "offset" Int32
                      :> QueryParam "to" Int32
                      :> Get '[JSON] (Vector HistoryReview)
            :<|> Auth :> HUser
                      :> "users"
                      :> "recommendations"
                      :> RequiredQueryParam "category" Category
                      :> QueryParam "offset" Int32
                      :> QueryParam "to" Int32
                      :> Get '[JSON] (Vector Recommendation)
