{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Services.Restaurant.Types
  ( CreateRestaurantReq(..)
  , CategoryFilter(..)
  , CreateItemReq(..)
  , CreateMenuReq(..)
  , Item(..)
  , MealType(..)
  , Menu(..)
  , MenuWithItems(..)
  , Restaurant(..)
  , ScoreFilter(..)
  , ItemWithScore(..)
  , SearchResult(..)
  ) where

import           Data.Aeson                     ( FromJSON(..)
                                                , ToJSON(..)
                                                )
import           Data.HashMap.Strict            ( HashMap )
import           Data.Scientific                ( Scientific )
import           Data.Set                       ( Set )
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import           Data.UUID                      ( UUID )
import           Data.Vector                    ( Vector )
import           GHC.Generics
import           Numeric.Natural                ( Natural )
import           Servant.API                    ( FromHttpApiData(..) )
import           Text.Read                      ( readMaybe )

import           Services.Reviews.Types         ( Category )

data CreateRestaurantReq = CreateRestaurantReq
  { email       :: Text
  , name        :: Text
  , country     :: Text
  , city        :: Text
  , address     :: Text
  , phoneNumber :: Text
  , description :: Text
  }
  deriving (Eq, Show, Generic)

instance FromJSON CreateRestaurantReq

data Restaurant = Restaurant
  { idx         :: UUID
  , adminId     :: UUID
  , imageId     :: Maybe UUID
  , email       :: Text
  , name        :: Text
  , country     :: Text
  , city        :: Text
  , address     :: Text
  , phoneNumber :: Text
  , description :: Text
  }
  deriving (Show, Generic)

instance Eq Restaurant where
  (==) (Restaurant id1 _ _ _ _ _ _ _ _ _) (Restaurant id2 _ _ _ _ _ _ _ _ _) =
    id1 == id2

instance Ord Restaurant where
  compare (Restaurant id1 _ _ _ _ _ _ _ _ _) (Restaurant id2 _ _ _ _ _ _ _ _ _)
    = compare id1 id2

instance ToJSON Restaurant

data CreateMenuReq = CreateMenuReq
  { title     :: Text
  , mealTypes :: Vector Text
  }
  deriving (Eq, Show, Generic)

instance FromJSON CreateMenuReq

data MealType = MealType
  { idx    :: UUID
  , menuId :: UUID
  , title  :: Text
  }
  deriving (Eq, Show, Generic)

instance ToJSON MealType

data Menu = Menu
  { idx          :: UUID
  , restaurantId :: UUID
  , mealTypes    :: Vector MealType
  , title        :: Text
  }
  deriving (Eq, Show, Generic)

instance ToJSON Menu

data MenuWithItems = MenuWithItems
  { menu  :: Menu
  , items :: HashMap UUID (Vector ItemWithScore)
  }
  deriving (Eq, Show, Generic)

instance ToJSON MenuWithItems

data CreateItemReq = CreateItemReq
  { title       :: Text
  , description :: Text
  , price       :: Scientific
  }
  deriving (Eq, Show, Generic)

instance FromJSON CreateItemReq

data Item = Item
  { idx         :: UUID
  , title       :: Text
  , imageId     :: Maybe UUID
  , description :: Text
  , price       :: Scientific
  , mealTypeId  :: UUID
  }
  deriving (Show, Ord, Generic)

instance ToJSON Item

instance Eq Item where
  (Item id0 _ _ _ _ _) == (Item id1 _ _ _ _ _) = id0 == id1

data ItemWithScore = ItemWithScore
  { item        :: Item
  , globalScore :: Float
  }
  deriving (Show, Ord, Generic)

instance ToJSON ItemWithScore

instance Eq ItemWithScore where
  (ItemWithScore i0 _) == (ItemWithScore i1 _) = i0 == i1

newtype CategoryFilter = CategoryFilter [Category]
newtype ScoreFilter = ScoreFilter [Natural]

instance FromHttpApiData CategoryFilter where
  parseQueryParam txt = case readMaybe (T.unpack txt) of
    Just filters -> Right $ CategoryFilter filters
    Nothing      -> Left "Could not parse query param for `CategoryFilter`"

instance FromHttpApiData ScoreFilter where
  parseQueryParam txt = case readMaybe (T.unpack txt) of
    Just filters -> Right $ ScoreFilter filters
    Nothing      -> Left "Could not parse query param for `ScoreFilter`"

data SearchResult = SearchResult
  { restaurants :: Set Restaurant
  }
  deriving (Show, Generic)

instance ToJSON SearchResult
