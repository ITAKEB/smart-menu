{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module Services.Restaurant.API where

import           Codec.Picture.Types            ( DynamicImage )
import           Data.Text                      ( Text )
import           Data.UUID                      ( UUID )
import           Data.Vector                    ( Vector )
import           Numeric.Natural                ( Natural )
import           Servant                        ( (:<|>)
                                                , (:>)
                                                , Capture
                                                , Get
                                                , Header'
                                                , JSON
                                                , Post
                                                , QueryParam'
                                                , ReqBody
                                                , Required
                                                )
import           Servant.JuicyPixels            ( JPEG )

import           Services.Restaurant.Types      ( CategoryFilter
                                                , CreateItemReq
                                                , CreateMenuReq
                                                , CreateRestaurantReq
                                                , Item
                                                , ItemWithScore
                                                , Menu
                                                , MenuWithItems
                                                , Restaurant
                                                , ScoreFilter
                                                , SearchResult
                                                )
import           Utils.JWT                      ( EncodedJWT )



type Auth = Header' '[Required] "Authorization" EncodedJWT
type HUser = Header' '[Required] "User" UUID
type RequiredQueryParam = QueryParam' '[Required]

type RestaurantAPI = Auth :> HUser :> "restaurants" :> ReqBody '[JSON] CreateRestaurantReq :> Post '[JSON] Restaurant
                :<|> Auth :> HUser :> "restaurants" :> Get '[JSON] (Vector Restaurant)
                :<|> Auth :> HUser :> "restaurants" :> Capture "restaurant-id" UUID :> "menus" :> ReqBody '[JSON] CreateMenuReq :> Post '[JSON] Menu
                :<|> Auth :> HUser :> "restaurants" :> "menus" :> "mealtypes"
                                   :> Capture "mealtype-id" UUID :> "items" :> ReqBody '[JSON] CreateItemReq :> Post '[JSON] Item
                :<|> Auth :> HUser
                          :> "restaurants"
                          :> Capture "restaurant-id" UUID
                          :> ReqBody '[JPEG 100] DynamicImage :> Post '[JSON] UUID
                :<|> Auth :> HUser
                          :> "restaurants"
                          :> "menus" :> "items"
                          :> Capture "item-id" UUID
                          :> ReqBody '[JPEG 100] DynamicImage :> Post '[JSON] UUID
                -- No auth endpoints
                :<|> "restaurants" :> Capture "restaurant-id" UUID :> Get '[JSON] Restaurant
                :<|> "restaurants" :> Capture "restaurant-id" UUID :> "menus" :> Get '[JSON] MenuWithItems
                :<|> "restaurants" :> Capture "restaurant-id" UUID :> "has-menu" :> Get '[JSON] Bool
                :<|> "restaurants" :> "menus"
                  :> "mealtypes" :> Capture "mealtype-id" UUID
                  :> "items"
                  :> RequiredQueryParam "categories" CategoryFilter
                  :> RequiredQueryParam "scores" ScoreFilter
                  :> "category-search" :> Get '[JSON] (Vector ItemWithScore)
                :<|> "restaurants" :> "menus"
                  :> "mealtypes" :> Capture "mealtype-id" UUID
                  :> "global-search"
                  :> RequiredQueryParam "score" Natural
                  :> "items" :> Get '[JSON] (Vector ItemWithScore)
                :<|> "restaurants" :> "search" :> RequiredQueryParam "query" Text :> Get '[JSON] SearchResult
