{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE FlexibleContexts           #-}

module Services.Restaurant.Handlers
  ( handlers
  ) where

import           Codec.Picture                  ( DynamicImage(ImageYCbCr8)
                                                , encodeJpegAtQuality
                                                )
import           Control.Monad                  ( when )
import           Control.Monad.Except           ( throwError )
import           Control.Monad.IO.Class         ( liftIO )
import           Control.Monad.Reader           ( asks )
import qualified Data.ByteString               as SB
import qualified Data.ByteString.Lazy          as LB
import qualified Data.HashMap.Strict           as HM
import           Data.List                      ( sort )
import           Data.Maybe                     ( fromJust
                                                , isJust
                                                , isNothing
                                                )
import qualified Data.Set                      as S
import qualified Data.Text                     as T
import           Data.Text                      ( Text )
import           Data.UUID                      ( UUID )
import           Data.UUID.V4                   ( nextRandom )
import qualified Data.Vector                   as V
import           Data.Vector                    ( Vector )
import           Hasql.Connection               ( Connection )
import           Hasql.Session                  ( Session )
import           Numeric.Natural                ( Natural )
import           Servant                        ( (:<|>)(..)
                                                , ServerT
                                                )


import           Data.Image.CRUD                ( insertImage )
import           Data.Restaurant.CRUD           ( insertItem
                                                , insertMenu
                                                , insertRestaurant
                                                , selectItemAverages
                                                , selectItemsByGlobalScore
                                                , selectItemsByMealTypeId
                                                , selectMenuByRestaurantId
                                                , selectRestaurantById
                                                , selectRestaurantsByAdminId
                                                , selectRestaurantsByWord
                                                , updateItemImageId
                                                , updateRestaurantImageId
                                                )
import           Data.Utils                     ( runSession )
import           Env.Types                      ( APIError
                                                  ( ImageTooLarge
                                                  , InvalidImgFormat
                                                  , MenuNotFound
                                                  , RestaurantNotFound
                                                  )
                                                , APIHandler
                                                , APIHandlerE
                                                , Env(dbConn)
                                                )
import           Env.Utils                      ( actionE
                                                , liftSession
                                                )
import           Services.Restaurant.API        ( RestaurantAPI )
import           Services.Restaurant.Types      ( CategoryFilter(..)
                                                , ScoreFilter(..)
                                                , SearchResult(..)
                                                )
import qualified Services.Restaurant.Types     as MLT
                                                ( MealType(..) )
import qualified Services.Restaurant.Types     as CIR
                                                ( CreateItemReq(..) )
import qualified Services.Restaurant.Types     as ITM
                                                ( Item(..) )
import qualified Services.Restaurant.Types     as ITS
                                                ( ItemWithScore(..) )
import qualified Services.Restaurant.Types     as MNU
                                                ( Menu(..) )
import qualified Services.Restaurant.Types     as MNI
                                                ( MenuWithItems(..) )
import qualified Services.Restaurant.Types     as CMR
                                                ( CreateMenuReq(..) )
import qualified Services.Restaurant.Types     as CRR
                                                ( CreateRestaurantReq(..) )
import qualified Services.Restaurant.Types     as RST
                                                ( Restaurant(..) )
import           Services.Reviews.Types         ( Category(..) )
import           Services.User.Auth             ( withAdmin )
import           Utils.JWT                      ( EncodedJWT )


handlers :: ServerT RestaurantAPI APIHandler
handlers =
  postRestaurantH
    :<|> getRestaurantsByAdminH
    :<|> postMenuH
    :<|> postItemH
    :<|> postRestaurantImgH
    :<|> postItemImgH
    :<|> getRestaurantH
    :<|> getRestaurantMenuH
    :<|> getHasMenuH
    :<|> getItemsByFiltersH
    :<|> getItemsByGlobalAvg
    :<|> getRestaurantsByQuery

--
-- Handler definitions
--
postRestaurantH
  :: EncodedJWT -> UUID -> CRR.CreateRestaurantReq -> APIHandler RST.Restaurant
postRestaurantH jwt' idx req = withAdmin idx jwt' $ \_ -> do
  conn <- asks dbConn
  rid  <- liftIO nextRandom
  let restaurant = RST.Restaurant { RST.idx         = rid
                                  , RST.adminId     = idx
                                  , RST.imageId     = Nothing
                                  , RST.email       = CRR.email req
                                  , RST.name        = CRR.name req
                                  , RST.country     = CRR.country req
                                  , RST.city        = CRR.city req
                                  , RST.address     = CRR.address req
                                  , RST.phoneNumber = CRR.phoneNumber req
                                  , RST.description = CRR.description req
                                  }
  liftSession $ runSession conn $ insertRestaurant $ restaurant
  return restaurant

postRestaurantImgH
  :: EncodedJWT -> UUID -> UUID -> DynamicImage -> APIHandler UUID
postRestaurantImgH jwt' uid rid img =
  postImg jwt' uid rid img updateRestaurantImageId

postMenuH
  :: EncodedJWT -> UUID -> UUID -> CMR.CreateMenuReq -> APIHandler MNU.Menu
postMenuH jwt' uid rid req = withAdmin uid jwt' $ \_ -> do
  conn      <- asks dbConn
  mid       <- liftIO nextRandom
  mealTypes <- mapM (mkMealType mid) $ CMR.mealTypes req
  let menu = MNU.Menu { MNU.idx          = mid
                      , MNU.restaurantId = rid
                      , MNU.mealTypes    = mealTypes
                      , MNU.title        = CMR.title req
                      }
  liftSession $ runSession conn $ insertMenu menu
  return menu
 where
  mkMealType :: UUID -> Text -> APIHandlerE MLT.MealType
  mkMealType menuId t = do
    idx <- liftIO nextRandom
    return MLT.MealType { MLT.idx = idx, MLT.title = t, MLT.menuId = menuId }

postItemH
  :: EncodedJWT -> UUID -> UUID -> CIR.CreateItemReq -> APIHandler ITM.Item
postItemH jwt' uid mtid req = withAdmin uid jwt' $ \_ -> do
  conn <- asks dbConn
  itid <- liftIO nextRandom
  let item = ITM.Item { ITM.idx         = itid
                      , ITM.title       = CIR.title req
                      , ITM.imageId     = Nothing
                      , ITM.description = CIR.description req
                      , ITM.price       = CIR.price req
                      , ITM.mealTypeId  = mtid
                      }
  liftSession $ runSession conn $ insertItem item
  return item

postItemImgH :: EncodedJWT -> UUID -> UUID -> DynamicImage -> APIHandler UUID
postItemImgH jwt' uid rid img = postImg jwt' uid rid img updateItemImageId

getRestaurantMenuH :: UUID -> APIHandler MNI.MenuWithItems
getRestaurantMenuH rid = actionE $ do
  conn      <- asks dbConn
  maybeMenu <- liftSession $ runSession conn $ selectMenuByRestaurantId rid
  when (isNothing maybeMenu) (throwError MenuNotFound)
  let menu = fromJust maybeMenu
  items <- mapM (fetchItems conn) (MNU.mealTypes menu)
  return $ MNI.MenuWithItems { MNI.menu  = menu
                             , MNI.items = HM.fromList $ V.toList items
                             }
 where
  fetchItems
    :: Connection
    -> MLT.MealType
    -> APIHandlerE (UUID, Vector ITS.ItemWithScore)
  fetchItems conn mt = do
    let mtid = MLT.idx mt
    items <- liftSession $ runSession conn $ selectItemsByMealTypeId mtid
    return (mtid, items)

getRestaurantsByAdminH
  :: EncodedJWT -> UUID -> APIHandler (Vector RST.Restaurant)
getRestaurantsByAdminH jwt' idx = withAdmin idx jwt' $ \_ -> do
  conn <- asks dbConn
  liftSession $ runSession conn $ selectRestaurantsByAdminId idx

getRestaurantH :: UUID -> APIHandler RST.Restaurant
getRestaurantH idx = actionE $ do
  conn            <- asks dbConn
  maybeRestaurant <- liftSession $ runSession conn $ selectRestaurantById idx
  when (isNothing maybeRestaurant) (throwError RestaurantNotFound)
  return $ fromJust maybeRestaurant

getHasMenuH :: UUID -> APIHandler Bool
getHasMenuH rid = actionE $ do
  conn      <- asks dbConn
  maybeMenu <- liftSession $ runSession conn $ selectMenuByRestaurantId rid
  return $ isJust maybeMenu

getItemsByFiltersH
  :: UUID
  -> CategoryFilter
  -> ScoreFilter
  -> APIHandler (Vector ITS.ItemWithScore)
getItemsByFiltersH mtId (CategoryFilter cats) (ScoreFilter scores) =
  actionE $ do
    let filters = zipWith (\c s -> (c, fromIntegral s, fromIntegral s + 1.0))
                          cats
                          scores
    conn  <- asks dbConn
    items <- liftSession $ runSession conn queryItemsAndAverages
    return $ fmap fst (V.filter (p filters) items)
 where
  p
    :: [(Category, Float, Float)]
    -> (ITS.ItemWithScore, [(Category, Float)])
    -> Bool
  p filters (_, avgs) =
    let f (_, minS, maxS) (_, s) = (minS, maxS, s)
        scores' = zipWith f (sort filters) (sort avgs)
    in  all (\(minS, maxS, s) -> s >= minS && s < maxS) scores'
  --
  queryItemsAndAverages = do
    items <- selectItemsByMealTypeId mtId
    let f i = do
          avgs <- selectItemAverages (ITM.idx $ ITS.item i) cats
          return (i, avgs)
    mapM f items

getItemsByGlobalAvg :: UUID -> Natural -> APIHandler (Vector ITS.ItemWithScore)
getItemsByGlobalAvg mtId globalAvg = actionE $ do
  conn <- asks dbConn
  liftSession $ runSession conn $ selectItemsByGlobalScore
    mtId
    (fromIntegral globalAvg)
    (fromIntegral globalAvg + 1)

getRestaurantsByQuery :: Text -> APIHandler SearchResult
getRestaurantsByQuery query = actionE $ do
  let words' = filter ((> 3) . T.length) (T.words query)
      stems  = simplify <$> words'
  conn   <- asks dbConn
  result <- liftSession $ runSession conn $ queryActions (query : stems)
  return $ SearchResult $ S.fromList $ V.toList result
 where
  queryActions ws = do
    rss <- mapM (selectRestaurantsByWord (0, 5)) ws
    return $ V.concat rss
  --
  simplify :: Text -> Text
  simplify w | len >= 11             = T.take (len - 4) w
             | len >= 8 && len <= 10 = T.take (len - 3) w
             | len >= 6 && len <= 7  = T.take (len - 2) w
             | len == 5              = T.take (len - 1) w
             | otherwise             = w
    where len = T.length w


--
-- Helpers
--
postImg
  :: EncodedJWT
  -> UUID
  -> UUID
  -> DynamicImage
  -> (UUID -> UUID -> Session ())
  -> APIHandler UUID
postImg jwt' uid idx (ImageYCbCr8 imgJPEG) update = withAdmin uid jwt' $ \_ ->
  do
    let encodedImg = LB.toStrict $ encodeJpegAtQuality 65 imgJPEG
        imgSize    = SB.length encodedImg
        maxSize    = 800000 -- 800KB
    when (imgSize > maxSize) (throwError ImageTooLarge)
    imgId <- liftIO nextRandom
    let dbActions = insertImage imgId encodedImg >> update idx imgId
    conn <- asks dbConn
    liftSession $ runSession conn dbActions
    return imgId
postImg jwt' uid _ _ _ = withAdmin uid jwt' $ \_ -> throwError InvalidImgFormat
