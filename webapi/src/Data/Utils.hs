{-# LANGUAGE OverloadedStrings #-}

module Data.Utils where

import           Data.Int                       ( Int16
                                                , Int32
                                                )
import           Data.Text                      ( Text )
import qualified Data.Text.Encoding            as T
                                                ( encodeUtf8 )
import           Data.UUID                      ( UUID )
import           Hasql.Connection               ( Connection )
import           Hasql.Session                  ( QueryError
                                                , Session
                                                , run
                                                )
import           Hasql.Statement                ( Statement(..) )
import qualified Services.Reviews.Types        as RV
                                                ( Review(..) )
import           Services.Reviews.Types         ( Review )

--
-- Run a session
--
runSession :: Connection -> Session a -> IO (Either QueryError a)
runSession conn s = run s conn

--
-- Ad hoc where clauses
--
where_ :: Statement a b -> Text -> Statement a b
where_ (Statement sql enc dec b) cond =
  Statement (sql <> " WHERE " <> T.encodeUtf8 cond) enc dec b

tupleToReview
  :: (UUID, UUID, UUID, Int16, Float, Maybe Text, Text, Int32) -> Review
tupleToReview (a, b, c, d, e, f, g, h) = RV.Review
  { RV.idx      = a
  , RV.userId   = b
  , RV.itemId   = c
  , RV.category = toEnum $ fromIntegral d
  , RV.score    = e
  , RV.content  = f
  , RV.userName = g
  , RV.likes    = fromIntegral h
  }
