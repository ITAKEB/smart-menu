{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Data.Image.CRUD
  ( insertImage
  , selectImageById
  ) where

import           Data.ByteString                ( ByteString )
import           Data.UUID                      ( UUID )
import           Hasql.Session                  ( Session
                                                , statement
                                                )
import           Hasql.Statement                ( Statement )
import           Hasql.TH                       ( maybeStatement
                                                , resultlessStatement
                                                )


insertImage :: UUID -> ByteString -> Session ()
insertImage imgId content = statement (imgId, content) query
 where
  query :: Statement (UUID, ByteString) ()
  query = [resultlessStatement|
        INSERT INTO image (id, content)
        VALUES ($1 :: uuid, $2 :: bytea)
    |]

selectImageById :: UUID -> Session (Maybe ByteString)
selectImageById imgId = statement imgId query
 where
  query :: Statement UUID (Maybe ByteString)
  query = [maybeStatement|
          SELECT content :: bytea FROM image
          WHERE id = $1 :: uuid
        |]
