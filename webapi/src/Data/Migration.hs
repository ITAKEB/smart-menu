{-# LANGUAGE DeriveGeneric     #-}

module Data.Migration where

import           Data.Aeson                     ( FromJSON )
import           Data.Word                      ( Word16 )
import           Database.PostgreSQL.Simple     ( ConnectInfo(..)
                                                , Connection
                                                , close
                                                , connect
                                                , withTransaction
                                                )
import           Database.PostgreSQL.Simple.Migration
                                                ( MigrationCommand(..)
                                                , MigrationContext(..)
                                                , MigrationResult
                                                  ( MigrationError
                                                  , MigrationSuccess
                                                  )
                                                , runMigration
                                                )
import           GHC.Generics


data Opts = Opts
  { pqHost     :: String
  , pqPort     :: Word16
  , pqUser     :: String
  , pqPassword :: String
  , pqDatabase :: String
  , migDir     :: FilePath
  }
  deriving (Show, Generic)

instance FromJSON Opts

runMigrations :: Connection -> FilePath -> IO ()
runMigrations conn migPath = do
  result <- runCommand MigrationInitialization conn
  case result of
    MigrationSuccess ->
      runCommandErr (MigrationDirectory migPath) conn >> closeConnection conn
    MigrationError _ ->
      runCommandErr
          (MigrationCommands
            [MigrationInitialization, MigrationDirectory migPath]
          )
          conn
        >> closeConnection conn

mkConnection :: Opts -> IO Connection
mkConnection opts = connect $ ConnectInfo (pqHost opts)
                                          (pqPort opts)
                                          (pqUser opts)
                                          (pqPassword opts)
                                          (pqDatabase opts)

closeConnection :: Connection -> IO ()
closeConnection = close

--
-- Helpers
--
runCommand :: MigrationCommand -> Connection -> IO (MigrationResult String)
runCommand cmd conn =
  withTransaction conn $ runMigration $ MigrationContext cmd True conn

runCommandErr :: MigrationCommand -> Connection -> IO ()
runCommandErr cmd conn = do
  result <- runCommand cmd conn
  case result of
    MigrationSuccess   -> return ()
    MigrationError err -> error err
