{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Data.Reviews.CRUD
  ( insertReview
  , updateLike
  , selectAverageRating
  , selectItemReviews
  , selectReviewNumLikes
  , selectUserLastReviewTime
  ) where


import           Data.Int                       ( Int16
                                                , Int32
                                                )
import           Data.Profunctor.Unsafe         ( rmap )
import           Data.Text                      ( Text )
import           Data.Time.Clock                ( UTCTime )
import           Data.UUID                      ( UUID )
import           Data.Vector                    ( Vector )
import qualified Data.Vector                   as V
import           Hasql.Session                  ( Session
                                                , statement
                                                )
import           Hasql.Statement                ( Statement )
import           Hasql.TH                       ( maybeStatement
                                                , resultlessStatement
                                                , singletonStatement
                                                , vectorStatement
                                                )
import           Numeric.Natural                ( Natural )


import           Data.Utils                     ( tupleToReview )
import           Services.Reviews.Types         ( Category
                                                , Review
                                                )


insertReview
  :: UUID -> UUID -> UUID -> Category -> Float -> Maybe Text -> Session ()
insertReview rId uId iId cat score cnt =
  let params = (rId, uId, iId, fromIntegral $ fromEnum cat, score, cnt)
  in  statement params query
 where
  query :: Statement (UUID, UUID, UUID, Int16, Float, Maybe Text) ()
  query = [resultlessStatement|
        INSERT INTO review (id, user_id, item_id, category, score, content)
        VALUES ($1 :: uuid,
                $2 :: uuid,
                $3 :: uuid,
                $4 :: int2,
                $5 :: float4,
                $6 :: text?)
    |]

selectAverageRating :: UUID -> Category -> Session (Maybe Float)
selectAverageRating iId cat =
  let params = (iId, fromIntegral $ fromEnum cat) in statement params query
 where
  query :: Statement (UUID, Int16) (Maybe Float)
  query = [singletonStatement|
           SELECT AVG(score) :: float4? FROM review
           WHERE item_id = $1 :: uuid AND category = $2 :: int2
        |]

selectUserLastReviewTime :: UUID -> UUID -> Category -> Session (Maybe UTCTime)
selectUserLastReviewTime uId iId cat = do
  let params = (uId, iId, fromIntegral $ fromEnum cat)
  result <- statement params query
  return (result V.!? 0)
 where
  query :: Statement (UUID, UUID, Int16) (Vector UTCTime)
  query = [vectorStatement|
          SELECT created_at :: timestamptz FROM review
          WHERE user_id = $1 :: uuid AND item_id = $2 :: uuid AND category = $3 :: int2
          ORDER BY created_at DESC
          LIMIT 1
        |]

selectItemReviews
  :: UUID -> Category -> (Int32, Int32) -> Session (Vector Review)
selectItemReviews iId cat (offset, n) =
  let params = (iId, fromIntegral $ fromEnum cat, offset, n)
  in  statement params $ rmap (fmap tupleToReview) query
 where
  query
    :: Statement
         (UUID, Int16, Int32, Int32)
         (Vector (UUID, UUID, UUID, Int16, Float, Maybe Text, Text, Int32))
  query = [vectorStatement|
             SELECT  rv.id       :: uuid,
                     rv.user_id  :: uuid,
                     rv.item_id  :: uuid,
                     rv.category :: int2,
                     rv.score    :: float4,
                     rv.content  :: text?,
                     u.name      :: text,
                     COUNT(rl.id) :: int4
             FROM review rv
             INNER JOIN xuser u ON rv.user_id = u.id
             LEFT JOIN review_like rl ON  rv.id = rl.review_id
             WHERE rv.item_id = $1 :: uuid AND
                   rv.category = $2 :: int2
             GROUP BY rv.id, u.name
             ORDER BY COUNT(rl.id) DESC
             OFFSET $3 :: int4
             LIMIT  $4 :: int4
             |]

updateLike :: UUID -> UUID -> UUID -> Session Bool
updateLike lId uId rId = do
  maybeLike <- statement (uId, rId) queryLike
  case maybeLike of
    Nothing        -> statement (lId, uId, rId) insert >> return False
    Just isUnliked -> do
      statement (uId, rId, not isUnliked) update
      return $ not isUnliked
 where
  queryLike :: Statement (UUID, UUID) (Maybe Bool)
  queryLike = [maybeStatement|
        SELECT is_unliked :: bool
        FROM review_like rl
        WHERE rl.user_id = $1 :: uuid AND rl.review_id = $2 :: uuid
    |]

  update :: Statement (UUID, UUID, Bool) ()
  update = [resultlessStatement|
        UPDATE review_like
        SET is_unliked = $3 :: bool
        WHERE user_id = $1 :: uuid AND review_id = $2 :: uuid
    |]

  insert :: Statement (UUID, UUID, UUID) ()
  insert = [resultlessStatement|
        INSERT INTO review_like (id, user_id, review_id)
        VALUES ($1 :: uuid,
                $2 :: uuid,
                $3 :: uuid)
    |]

selectReviewNumLikes :: UUID -> Session Natural
selectReviewNumLikes rId = statement rId $ rmap fromIntegral query
 where
  query :: Statement UUID Int32
  query = [singletonStatement|
           SELECT COUNT(*) :: int4 FROM review_like
           WHERE review_id = $1 :: uuid AND is_unliked = false
        |]
