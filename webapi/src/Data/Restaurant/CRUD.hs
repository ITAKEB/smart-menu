{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Data.Restaurant.CRUD
  ( insertRestaurant
  , insertItem
  , insertMealType
  , insertMenu
  , selectItemAverages
  , selectItemsByGlobalScore
  , selectItemsByMealTypeId
  , selectRestaurantById
  , updateItemImageId
  , selectMenuByRestaurantId
  , selectRestaurantsByAdminId
  , updateRestaurantImageId
  , selectRestaurantsByWord
  ) where

import           Data.Int                       ( Int16
                                                , Int32
                                                )
import           Data.Maybe                     ( fromMaybe )
import           Data.Profunctor.Unsafe         ( rmap )
import           Data.Scientific                ( Scientific )
import           Data.Text                      ( Text )
import           Data.UUID                      ( UUID )
import           Data.Vector                    ( Vector )
import           Hasql.Session                  ( Session
                                                , statement
                                                )
import           Hasql.Statement                ( Statement )
import           Hasql.TH                       ( maybeStatement
                                                , resultlessStatement
                                                , singletonStatement
                                                , vectorStatement
                                                )

import qualified Services.Restaurant.Types     as ITM
                                                ( Item(..) )
import qualified Services.Restaurant.Types     as ITS
                                                ( ItemWithScore(..) )
import qualified Services.Restaurant.Types     as MNU
                                                ( Menu(..) )
import qualified Services.Restaurant.Types     as MLT
                                                ( MealType(..) )
import qualified Services.Restaurant.Types     as RST
                                                ( Restaurant(..) )
import           Services.Reviews.Types         ( Category(..) )


insertRestaurant :: RST.Restaurant -> Session ()
insertRestaurant restaurant =
  let params =
        ( RST.idx restaurant
        , RST.adminId restaurant
        , RST.email restaurant
        , RST.name restaurant
        , RST.country restaurant
        , RST.city restaurant
        , RST.address restaurant
        , RST.phoneNumber restaurant
        , RST.description restaurant
        )
  in  statement params query
 where
  query :: Statement (UUID, UUID, Text, Text, Text, Text, Text, Text, Text) ()
  query = [resultlessStatement|
        INSERT INTO restaurant (id, admin_id, email, name, country, city, address, phone_number, description)
        VALUES ($1 :: uuid,
                $2 :: uuid,
                $3 :: text,
                $4 :: text,
                $5 :: text,
                $6 :: text,
                $7 :: text,
                $8 :: text,
                $9 :: text)
    |]

updateRestaurantImageId :: UUID -> UUID -> Session ()
updateRestaurantImageId rId imgId = statement (rId, imgId) query
 where
  query :: Statement (UUID, UUID) ()
  query = [resultlessStatement|
        UPDATE restaurant
        SET image_id = $2 :: uuid
        WHERE restaurant.id = $1 :: uuid
    |]

updateItemImageId :: UUID -> UUID -> Session ()
updateItemImageId iId imgId = statement (iId, imgId) query
 where
  query :: Statement (UUID, UUID) ()
  query = [resultlessStatement|
        UPDATE item
        SET image_id = $2 :: uuid
        WHERE item.id = $1 :: uuid
    |]

selectRestaurantById :: UUID -> Session (Maybe RST.Restaurant)
selectRestaurantById idx = statement idx $ rmap (fmap tupleToRestaurant) query
 where
  query
    :: Statement
         UUID
         ( Maybe
             (UUID, UUID, Maybe UUID, Text, Text, Text, Text, Text, Text, Text)
         )
  query = [maybeStatement|
        SELECT id           :: uuid,
               admin_id     :: uuid,
               image_id     :: uuid?,
               name         :: text,
               email        :: text,
               city         :: text,
               country      :: text,
               address      :: text,
               phone_number :: text,
               description  :: text
        FROM restaurant
        WHERE id = $1 :: uuid
    |]

selectRestaurantsByAdminId :: UUID -> Session (Vector RST.Restaurant)
selectRestaurantsByAdminId idx = statement idx
  $ rmap (fmap tupleToRestaurant) query
 where
  query
    :: Statement
         UUID
         ( Vector
             (UUID, UUID, Maybe UUID, Text, Text, Text, Text, Text, Text, Text)
         )
  query = [vectorStatement|
        SELECT r.id           :: uuid,
               r.admin_id     :: uuid,
               r.image_id     :: uuid?,
               r.name         :: text,
               r.email        :: text,
               r.city         :: text,
               r.country      :: text,
               r.address      :: text,
               r.phone_number :: text,
               r.description  :: text
        FROM restaurant r
        INNER JOIN xuser u
        ON u.id = r.admin_id
        WHERE r.admin_id = $1 :: uuid
    |]

selectRestaurantsByWord
  :: (Int32, Int32) -> Text -> Session (Vector RST.Restaurant)
selectRestaurantsByWord (offset, n) word =
  statement ("%" <> word <> "%", offset, n)
    $ rmap (fmap tupleToRestaurant) query
 where
  query
    :: Statement
         (Text, Int32, Int32)
         ( Vector
             (UUID, UUID, Maybe UUID, Text, Text, Text, Text, Text, Text, Text)
         )
  query = [vectorStatement|
        SELECT r.id           :: uuid,
               r.admin_id     :: uuid,
               r.image_id     :: uuid?,
               r.name         :: text,
               r.email        :: text,
               r.city         :: text,
               r.country      :: text,
               r.address      :: text,
               r.phone_number :: text,
               r.description  :: text
        FROM restaurant r
        INNER JOIN xuser u
        ON u.id = r.admin_id
        WHERE r.name ILIKE $1 :: text
        OFFSET $2 :: int4
        LIMIT  $3 :: int4
    |]


selectMenuByRestaurantId :: UUID -> Session (Maybe MNU.Menu)
selectMenuByRestaurantId idx = do
  mealTypes <- statement idx $ rmap (fmap tupleToMealType) queryMealTypes
  menu      <- statement idx $ rmap (fmap $ tupleToMenu mealTypes idx) queryMenu
  return menu
 where
  queryMenu :: Statement UUID (Maybe (UUID, Text))
  queryMenu = [maybeStatement|
        SELECT id            :: uuid,
               title         :: text
        FROM menu
        WHERE menu.restaurant_id = $1 :: uuid
    |]
  --
  queryMealTypes :: Statement UUID (Vector (UUID, UUID, Text))
  queryMealTypes = [vectorStatement|
        SELECT mt.id           :: uuid,
               mt.menu_id      :: uuid,
               mt.title        :: text
        FROM meal_type mt
        INNER JOIN menu
        ON menu.id = mt.menu_id
        WHERE menu.restaurant_id = $1 :: uuid
    |]

selectItemsByMealTypeId :: UUID -> Session (Vector ITS.ItemWithScore)
selectItemsByMealTypeId idx = statement idx
  $ rmap (fmap tupleToItemWithScore) query
 where
  query
    :: Statement
         UUID
         (Vector (UUID, UUID, Maybe UUID, Text, Text, Scientific, Maybe Float))
  query = [vectorStatement|
        SELECT i.id           :: uuid,
               i.meal_type_id :: uuid,
               i.image_id     :: uuid?,
               i.title        :: text,
               i.description  :: text,
               i.price        :: numeric,
               AVG(r.score)   :: float4?
        FROM item i
        LEFT JOIN review r ON i.id = r.item_id
        WHERE i.meal_type_id = $1 :: uuid
        GROUP BY i.id
    |]

insertMenu :: MNU.Menu -> Session ()
insertMenu menu = do
  statement (MNU.idx menu, MNU.restaurantId menu, MNU.title menu) query
  mapM_ insertMealType $ MNU.mealTypes menu
 where
   --
  query :: Statement (UUID, UUID, Text) ()
  query = [resultlessStatement|
         INSERT INTO menu (id, restaurant_id, title)
         VALUES ($1 :: uuid,
                 $2 :: uuid,
                 $3 :: text)
     |]

insertMealType :: MLT.MealType -> Session ()
insertMealType mealType =
  let params = (MLT.idx mealType, MLT.menuId mealType, MLT.title mealType)
  in  statement params query
 where
  query :: Statement (UUID, UUID, Text) ()
  query = [resultlessStatement|
        INSERT INTO meal_type (id, menu_id, title)
        VALUES ($1 :: uuid,
                $2 :: uuid,
                $3 :: text)
    |]

insertItem :: ITM.Item -> Session ()
insertItem item =
  let params =
        ( ITM.idx item
        , ITM.mealTypeId item
        , ITM.title item
        , ITM.description item
        , ITM.price item
        )
  in  statement params query
 where
  query :: Statement (UUID, UUID, Text, Text, Scientific) ()
  query = [resultlessStatement|
        INSERT INTO item (id, meal_type_id, title, description, price)
        VALUES ($1 :: uuid,
                $2 :: uuid,
                $3 :: text,
                $4 :: text,
                $5 :: numeric)
    |]

selectItemAverages :: UUID -> [Category] -> Session [(Category, Float)]
selectItemAverages iId categories =
  let f c = do
        avg <- statement (iId, fromIntegral $ fromEnum c) query
        pure $ (c, fromMaybe 0 avg)
  in  mapM f categories
 where
  query :: Statement (UUID, Int16) (Maybe Float)
  query = [singletonStatement|
           SELECT AVG(score) :: float4? FROM review
           WHERE item_id = $1 :: uuid AND category = $2 :: int2
        |]

selectItemsByGlobalScore
  :: UUID -> Float -> Float -> Session (Vector ITS.ItemWithScore)
selectItemsByGlobalScore mtId minS maxS =
  let params = (mtId, minS, maxS)
  in  statement params $ rmap (fmap tupleToItemWithScore) query
 where
  query
    :: Statement
         (UUID, Float, Float)
         (Vector (UUID, UUID, Maybe UUID, Text, Text, Scientific, Maybe Float))
  query = [vectorStatement|
         SELECT i.id                 :: uuid,
                i.meal_type_id       :: uuid,
                i.image_id           :: uuid?,
                i.title              :: text,
                i.description        :: text,
                i.price              :: numeric,
                AVG(r.score)         :: float4?
         FROM item i
         LEFT JOIN review r ON i.id = r.item_id
         WHERE i.meal_type_id = $1 :: uuid
         GROUP BY i.id
         HAVING AVG(r.score) >= $2 :: float4 AND AVG(r.score) < $3 :: float4
     |]

--
-- Helpers
--
tupleToItemWithScore
  :: (UUID, UUID, Maybe UUID, Text, Text, Scientific, Maybe Float)
  -> ITS.ItemWithScore
tupleToItemWithScore (a0, a1, a2, a3, a4, a5, a6) = ITS.ItemWithScore
  { ITS.item        = tupleToItem (a0, a1, a2, a3, a4, a5)
  , ITS.globalScore = fromMaybe 0 a6
  }

tupleToItem :: (UUID, UUID, Maybe UUID, Text, Text, Scientific) -> ITM.Item
tupleToItem (a0, a1, a2, a3, a4, a5) = ITM.Item { ITM.idx         = a0
                                                , ITM.mealTypeId  = a1
                                                , ITM.imageId     = a2
                                                , ITM.title       = a3
                                                , ITM.description = a4
                                                , ITM.price       = a5
                                                }

tupleToMenu :: Vector MLT.MealType -> UUID -> (UUID, Text) -> MNU.Menu
tupleToMenu mealTypes restaurantId (a0, a1) = MNU.Menu
  { MNU.idx          = a0
  , MNU.restaurantId = restaurantId
  , MNU.mealTypes    = mealTypes
  , MNU.title        = a1
  }

tupleToMealType :: (UUID, UUID, Text) -> MLT.MealType
tupleToMealType (a0, a1, a2) =
  MLT.MealType { MLT.idx = a0, MLT.menuId = a1, MLT.title = a2 }

tupleToRestaurant
  :: (UUID, UUID, Maybe UUID, Text, Text, Text, Text, Text, Text, Text)
  -> RST.Restaurant
tupleToRestaurant (a0, a1, a2, a3, a4, a5, a6, a7, a8, a9) = RST.Restaurant
  { RST.idx         = a0
  , RST.adminId     = a1
  , RST.imageId     = a2
  , RST.name        = a3
  , RST.email       = a4
  , RST.city        = a5
  , RST.country     = a6
  , RST.address     = a7
  , RST.phoneNumber = a8
  , RST.description = a9
  }
