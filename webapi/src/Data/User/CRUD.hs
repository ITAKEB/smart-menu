{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Data.User.CRUD
  ( selectUserWithPasswordByEmail
  , insertUser
  , isUserAdmin
  , selectUserByEmail
  , selectAverageUserScore
  , selectItemRecommendations
  , selectUserJWKById
  , selectUserLastReviews
  , updateUserJWK
  ) where

import           Control.Monad.IO.Class         ( liftIO )
import           Data.ByteString                ( ByteString )
import qualified Data.ByteString.Lazy          as LBS
import           Data.Int                       ( Int16
                                                , Int32
                                                )
import           Data.Maybe                     ( fromMaybe )
import           Data.Profunctor.Unsafe         ( rmap )
import           Data.Scientific                ( Scientific )
import           Data.Text                      ( Text )
import           Data.Time.Calendar             ( Day )
import           Data.Time.Clock                ( UTCTime
                                                , getCurrentTime
                                                )
import           Data.UUID                      ( UUID )
import           Data.Vector                    ( Vector )
import           Hasql.Session                  ( Session
                                                , statement
                                                )
import           Hasql.Statement                ( Statement )
import           Hasql.TH                       ( maybeStatement
                                                , resultlessStatement
                                                , singletonStatement
                                                , vectorStatement
                                                )

import           Services.Reviews.Types         ( Category )
import qualified Services.User.Types           as RCM
                                                ( Recommendation(..) )
import qualified Services.User.Types           as USR
                                                ( User(..) )
import qualified Services.User.Types           as HRV
                                                ( HistoryReview(..) )
import qualified Services.User.Types           as HPS
                                                ( HashedPassword(..) )
import           Utils.JWT                      ( EncodedJWK )


selectUserJWKById :: UUID -> Session (Maybe ByteString)
selectUserJWKById idx = statement idx query
 where
  query :: Statement UUID (Maybe ByteString)
  query = [maybeStatement|
        SELECT jwk :: bytea
        FROM xuser
        WHERE id = $1 :: uuid
    |]

isUserAdmin :: UUID -> Session Bool
isUserAdmin idx = statement idx $ rmap (fromMaybe False) (query)
 where
  query :: Statement UUID (Maybe Bool)
  query = [maybeStatement|
        SELECT is_admin :: bool
        FROM xuser
        WHERE id = $1 :: uuid
    |]

updateUserJWK :: (UUID, Maybe EncodedJWK) -> Session ()
updateUserJWK (uid, jwk) = do
  now <- liftIO getCurrentTime
  statement (uid, LBS.toStrict <$> jwk, now) query
 where
  query :: Statement (UUID, Maybe ByteString, UTCTime) ()
  query = [resultlessStatement|
        UPDATE xuser
        SET jwk = $2 :: bytea?,
            updated_at = $3 :: timestamptz
        WHERE id = $1 :: uuid
    |]

insertUser :: USR.User -> HPS.HashedPassword -> Session ()
insertUser user pass =
  let params =
        ( USR.idx user
        , USR.email user
        , USR.name user
        , USR.city user
        , USR.country user
        , USR.birthdate user
        , USR.isAdmin user
        , HPS.getHash pass
        )
  in  statement params query
 where
  query :: Statement (UUID, Text, Text, Text, Text, Day, Bool, ByteString) ()
  query = [resultlessStatement|
        INSERT INTO xuser (id, email, name, city, country, birthdate, is_admin, password)
        VALUES ($1 :: uuid,
                $2 :: text,
                $3 :: text,
                $4 :: text,
                $5 :: text,
                $6 :: date,
                $7 :: bool,
                $8 :: bytea)
    |]

selectUserByEmail :: Text -> Session (Maybe USR.User)
selectUserByEmail email = statement email $ rmap (fmap tupleToUser) query
 where
  query :: Statement Text (Maybe (UUID, Text, Text, Text, Text, Bool, Day))
  query = [maybeStatement|
        SELECT id        :: uuid,
               name      :: text,
               email     :: text,
               city      :: text,
               country   :: text,
               is_admin  :: bool,
               birthdate :: date
        FROM xuser
        WHERE email = $1 :: text
    |]

selectUserWithPasswordByEmail
  :: Text -> Session (Maybe (USR.User, HPS.HashedPassword))
selectUserWithPasswordByEmail email =
  let f (a0, a1, a2, a3, a4, a5, a6, a7) =
        (tupleToUser (a0, a1, a2, a3, a4, a5, a6), HPS.HashedPassword a7)
  in  statement email $ rmap (fmap f) query
 where
  query
    :: Statement
         Text
         (Maybe (UUID, Text, Text, Text, Text, Bool, Day, ByteString))
  query = [maybeStatement|
        SELECT id        :: uuid,
               name      :: text,
               email     :: text,
               city      :: text,
               country   :: text,
               is_admin  :: bool,
               birthdate :: date,
               password  :: bytea
        FROM xuser
        WHERE email = $1 :: text
    |]

selectUserLastReviews
  :: UUID -> Category -> (Int32, Int32) -> Session (Vector HRV.HistoryReview)
selectUserLastReviews uId cat (offset, n) =
  let params = (uId, fromIntegral $ fromEnum cat, offset, n)
  in  statement params $ rmap (fmap tupleToHistoryReview) query
 where
  query
    :: Statement
         (UUID, Int16, Int32, Int32)
         ( Vector
             (Int16, Float, Maybe Text, Int32, Text, Maybe UUID, Scientific)
         )
  query = [vectorStatement|
             SELECT  rv.category  :: int2,
                     rv.score     :: float4,
                     rv.content   :: text?,
                     COUNT(rl.id) :: int4,
                     i.title      :: text,
                     i.image_id   :: uuid?,
                     i.price      :: numeric
             FROM review rv
             INNER JOIN xuser u ON rv.user_id = u.id
             INNER JOIN item i ON rv.item_id = i.id
             LEFT JOIN review_like rl ON rv.id = rl.review_id
             WHERE rv.user_id = $1 :: uuid AND rv.category = $2 :: int2
             GROUP BY rv.id, i.id
             ORDER BY rv.created_at DESC
             OFFSET $3 :: int4
             LIMIT  $4 :: int4
             |]

selectAverageUserScore :: UUID -> Category -> Session (Maybe Float)
selectAverageUserScore uId cat =
  let params = (uId, fromIntegral $ fromEnum cat) in statement params query
 where
  query :: Statement (UUID, Int16) (Maybe Float)
  query = [singletonStatement|
            SELECT AVG(score) :: float4? FROM review
            WHERE user_id = $1 :: uuid AND category = $2 :: int2
         |]

selectItemRecommendations
  :: Float -> Category -> (Int32, Int32) -> Session (Vector RCM.Recommendation)
selectItemRecommendations score cat (offset, n) =
  let params = (score, fromIntegral $ fromEnum cat, offset, n)
  in  statement params $ rmap (fmap tupleToRecommendation) query
 where
  query
    :: Statement
         (Float, Int16, Int32, Int32)
         (Vector (UUID, UUID, Float, Text, Maybe UUID, Scientific))
  query = [vectorStatement|
              SELECT i.meal_type_id   :: uuid,
                     mn.restaurant_id :: uuid,
                     AVG(rv.score)    :: float4,
                     i.title          :: text,
                     i.image_id       :: uuid?,
                     i.price          :: numeric
              FROM review rv
              INNER JOIN item i ON rv.item_id = i.id
              INNER JOIN meal_type mt ON i.meal_type_id = mt.id
              INNER JOIN menu mn ON mt.menu_id = mn.id
              WHERE rv.category = $2 :: int2
              GROUP BY mn.restaurant_id, i.id
              HAVING AVG(rv.score) >= $1 :: float4
              ORDER BY AVG(rv.score) DESC
              OFFSET $3 :: int4
              LIMIT  $4 :: int4
              |]

--
-- Helpers
--
tupleToRecommendation
  :: (UUID, UUID, Float, Text, Maybe UUID, Scientific) -> RCM.Recommendation
tupleToRecommendation (a0, a1, a2, a3, a4, a5) = RCM.Recommendation
  { RCM.itemTitle   = a3
  , RCM.imageId     = a4
  , RCM.price       = a5
  , RCM.score       = a2
  , RCM.resturantId = a1
  , RCM.mealTypeId  = a0
  }

tupleToUser :: (UUID, Text, Text, Text, Text, Bool, Day) -> USR.User
tupleToUser (a0, a1, a2, a3, a4, a5, a6) = USR.User { USR.idx       = a0
                                                    , USR.name      = a1
                                                    , USR.email     = a2
                                                    , USR.city      = a3
                                                    , USR.country   = a4
                                                    , USR.isAdmin   = a5
                                                    , USR.birthdate = a6
                                                    }

tupleToHistoryReview
  :: (Int16, Float, Maybe Text, Int32, Text, Maybe UUID, Scientific)
  -> HRV.HistoryReview
tupleToHistoryReview (a0, a1, a2, a3, a4, a5, a6) = HRV.HistoryReview
  { HRV.category  = toEnum $ fromIntegral a0
  , HRV.score     = a1
  , HRV.content   = a2
  , HRV.likes     = fromIntegral a3
  , HRV.itemTitle = a4
  , HRV.imageId   = a5
  , HRV.price     = a6
  }
