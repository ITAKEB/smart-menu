module Main where

import           Data.Yaml                      ( decodeFileEither )
import           Options.Applicative            ( Parser
                                                , ParserInfo
                                                , execParser
                                                , fullDesc
                                                , header
                                                , help
                                                , helper
                                                , info
                                                , long
                                                , metavar
                                                , progDesc
                                                , short
                                                , strOption
                                                )

import           Data.Migration                 ( Opts(migDir)
                                                , mkConnection
                                                , runMigrations
                                                )


newtype CmdOpts = CmdOpts
  { configPath :: String
  } deriving (Show)

main :: IO ()
main = do
  cmdOpts    <- execParser parserInfo
  eitherOpts <- decodeFileEither (configPath cmdOpts)
  case eitherOpts of
    Right opts -> do
      conn <- mkConnection opts
      runMigrations conn (migDir opts)
    Left e -> error $ show e

parserInfo :: ParserInfo CmdOpts
parserInfo = info
  (helper <*> optionParser)
  (fullDesc <> header "run-migration" <> progDesc "Run SmartMenu migrations")

optionParser :: Parser CmdOpts
optionParser = CmdOpts <$> strOption
  (long "config-path" <> short 'c' <> metavar "CONFIGPATH" <> help
    "Path to config-file"
  )
