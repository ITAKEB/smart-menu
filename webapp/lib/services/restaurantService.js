export async function postRestaurant(jwt, adminId, restaurantData) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: JSON.stringify(restaurantData)
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export function mkRestaurantData(email, name, country, city, address, phoneNumber, description) {
    return { email, name, country, city, address, phoneNumber, description }
}

export async function getRestaurants(jwt, adminId) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export async function getRestaurantById(restaurantId) {
    const host = process.env.API_HOST

    const response = await fetch(`${host}/restaurants/${restaurantId}`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin'
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export async function postMenu(jwt, adminId, restaurantId, menuData) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants/${restaurantId}/menus`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: JSON.stringify(menuData)
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export function mkMenuData(title, mealTypes) {
    return { title, mealTypes }
}

export async function getHasMenu(restaurantId) {
    const host = process.env.API_HOST

    const response = await fetch(`${host}/restaurants/${restaurantId}/has-menu`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin'
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.text()
        return data === 'true'
    }
}

export async function getMenuWithItems(restaurantId) {
    const host = process.env.API_HOST

    const response = await fetch(`${host}/restaurants/${restaurantId}/menus`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin'
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export async function postItem(jwt, adminId, mealTypeId, itemData) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants/menus/mealtypes/${mealTypeId}/items`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: JSON.stringify(itemData)
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export function mkItemData(title, description, price) {
    return { title, description, price }
}

export async function getItemsByGlobalScore(mealTypeId, globalScore) {
    const host = process.env.API_HOST

    const url = `${host}/restaurants/menus/mealtypes/${mealTypeId}/global-search/items?score=${globalScore}`

    const response = await fetch(url, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin'
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export async function getItemsByCategoriesScore(mealTypeId, categoryFilter, scoreFilter) {
    const host = process.env.API_HOST

    const url = `${host}/restaurants/menus/mealtypes/${mealTypeId}/items/category-search?categories=${categoryFilter}&scores=${scoreFilter}`

    const response = await fetch(url, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin'
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export async function postRestaurantImage(jwt, adminId, restaurantId, image) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'image/jpeg')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants/${restaurantId}`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: image
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export async function postItemImage(jwt, adminId, itemId, image) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'image/jpeg')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants/menus/items/${itemId}`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: image
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export async function getRestaurantByWord(search) {
    const host = process.env.API_HOST

    const url = `${host}/restaurants/search?query=${search}`

    const response = await fetch(url, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin'
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}
