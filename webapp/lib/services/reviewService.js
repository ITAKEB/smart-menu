export async function postReview(jwt, uid, itemId, reviewData) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', uid)

    const response = await fetch(`${host}/restaurants/menus/items/${itemId}/reviews`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: JSON.stringify(reviewData)
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        return null
    }
}

export function mkReview(category, content, score) {
    return { category, content, score }
}

export async function getAvarageRatings(itemId) {
    const host = process.env.API_HOST

    const response = await fetch(`${host}/restaurants/menus/items/${itemId}/ratings/average`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin'
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export async function getReviews(itemId, category, offset, to) {
    const host = process.env.API_HOST
    const url = `${host}/restaurants/menus/items/${itemId}/reviews?category=${category}&offset=${offset}&to=${to}`

    const response = await fetch(url, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin'
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export async function postLike(jwt, uid, reviewId) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', uid)

    const response = await fetch(`${host}/restaurants/menus/items/reviews/likes/${reviewId}`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const result = await response.json()
        return result
    }
}

export async function getLikes(reviewId) {
    const host = process.env.API_HOST
    const url = `${host}/restaurants/menus/items/reviews/likes/${reviewId}`

    const response = await fetch(url, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin'
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}
