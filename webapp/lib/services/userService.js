import { encode } from 'js-base64'

export async function postUser(userData) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')

    const response = await fetch(`${host}/users`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: JSON.stringify(userData)
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export function mkUser(email, name, password, city, country, birthdate, isAdmin) {
    return { email, name, password, city, country, birthdate, isAdmin }
}

export async function basicAuth(email, password) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.set('Authorization', 'Basic ' + encode(email + ':' + password))

    const response = await fetch(`${host}/users/token`, {
        method: 'PUT',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if (code == 401) {
        throw 'Unauthorized'
    }

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export async function revokeToken(jwt, uid) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.set('Authorization', 'Bearer ' + jwt)

    const response = await fetch(`${host}/users/${uid}/token/revoke`, {
        method: 'DELETE',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        return null
    }
}

export async function getUserHistory(jwt, adminId, category, offset, to) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/users/reviews?category=${category}&offset=${offset}&to=${to}`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export async function getUserRecommentations(jwt, adminId, category, offset, to) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/users/recommendations?category=${category}&offset=${offset}&to=${to}`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

/* 
:<|> Auth :> HUser
                      :> "users"
                      :> "score"
                      :> RequiredQueryParam "category" Category
                      :> Get '[JSON] UserScore

*/

export async function getUserPerformance(jwt, adminId, category) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/users/score?category=${category}`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}
