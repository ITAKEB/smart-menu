export function getImgUrl(imgId, w, h) {
    const host = process.env.API_HOST

    return `${host}/image/${imgId}?width=${w}&height=${h}`
}
