import { default as passwordValidator } from 'password-validator'
import validator from 'validator'

export function signUpValidator({ name, email, password, confirmPassword, country, city }) {
    return {
        nameValidation: validateName(name),
        emailValidation: validateEmail(email),
        passwordValidation: validatePassword(password, confirmPassword),
        countryValidation: validateName(country),
        cityValidation: validateName(city)
    }
}

export function satisfiesAllSignUpValidations({
    nameValidation,
    emailValidation,
    passwordValidation,
    cityValidation,
    countryValidation
}) {
    return (
        nameValidation.isValid &&
        emailValidation.isValid &&
        passwordValidation.isValid &&
        cityValidation.isValid &&
        countryValidation.isValid
    )
}

// Helpers
function validatePassword(password, confirmPassword) {
    const schema = new passwordValidator()
    const passSchema = schema
        .is()
        .min(8)
        .is()
        .max(30)
        .has()
        .uppercase()
        .has()
        .lowercase()
        .has()
        .digits(1)
        .has()
        .symbols()
        .has()
        .not()
        .spaces()
    const errors = passSchema.validate(password, { list: true })

    if (errors.includes('min') || errors.includes('max')) {
        return { isValid: false, message: 'debe tener entre 8 y 30 caracteres.' }
    }

    if (errors.includes('uppercase')) {
        return { isValid: false, message: 'debe tener un caracter en mayusucla.' }
    }

    if (errors.includes('lowercase')) {
        return { isValid: false, message: 'debe tener un caracter en minuscula.' }
    }

    if (errors.includes('digits')) {
        return { isValid: false, message: 'debe tener almenos un digito.' }
    }

    if (errors.includes('symbols')) {
        return { isValid: false, message: 'debe tener alemnos un caracter especial.' }
    }

    if (errors.includes('spaces')) {
        return { isValid: false, message: 'no puede tener espacios.' }
    }

    if (password != confirmPassword) {
        return { isValid: false, message: 'no coincide con la confimación.' }
    }

    return { isValid: true, message: null }
}

export function validateEmail(email) {
    if (email.length < 1) {
        return { isValid: false, message: 'no puede ser vacio.' }
    }

    if (!validator.isEmail(email)) {
        return { isValid: false, message: 'formato invalido.' }
    }

    return { isValid: true, message: null }
}

export function validateName(name) {
    const schema = new passwordValidator()
    const nameSchema = schema.is().min(2).is().max(45)
    const errors = nameSchema.validate(name, { list: true })

    if (errors.includes('min') || errors.includes('max')) {
        return { isValid: false, message: 'debe estar entre 2 y 45 caracteres.' }
    }

    return { isValid: true, message: null }
}

export function validateReview(text) {
    if (text.length > 500) {
        return { isValid: false, message: 'Debe tener menos de 500 caracteres.' }
    }

    return { isValid: true, message: null }
}

export function validatePhoneNumber(txt) {
    const reg = /^\(?\d{3}\)?((-)?|(\s|-)?)\d{3}(\s|-)?\d{2}(\s|-)?\d{2}$/

    if (!reg.test(txt)) {
        return { isValid: false, message: 'formato invalido.' }
    }

    return { isValid: true, message: null }
}

export function validateDescription(text) {
    if (text.length > 500) {
        return { isValid: false, message: 'debe tener menos de 500 caracteres.' }
    }
    if (text.length < 1) {
        return { isValid: false, message: 'debe proporcionar descripción.' }
    }

    return { isValid: true, message: null }
}

export function registerRestaurantValidator({ name, email, country, city, address, phoneNumber, description }) {
    return {
        nameValidation: validateName(name),
        emailValidation: validateEmail(email),
        countryValidation: validateName(country),
        cityValidation: validateName(city),
        addressValidation: validateName(address),
        phoneNumberValidation: validatePhoneNumber(phoneNumber),
        descriptionValidation: validateDescription(description)
    }
}

export function satisfiesAllRegisterRestaurantValidation({
    nameValidation,
    emailValidation,
    cityValidation,
    countryValidation,
    addressValidation,
    phoneNumberValidation,
    descriptionValidation
}) {
    return (
        nameValidation.isValid &&
        emailValidation.isValid &&
        phoneNumberValidation.isValid &&
        cityValidation.isValid &&
        descriptionValidation.isValid &&
        addressValidation.isValid &&
        countryValidation.isValid
    )
}

export function validatePrice(text) {
    const reg = /^\d+$/

    if (text.length < 1) {
        return { isValid: false, message: 'precio es requerido.' }
    }
    if (!reg.test(text)) {
        return { isValid: false, message: 'debe ser un numero.' }
    }

    return { isValid: true, message: null }
}

export function registerItemValidator({ title, description, mealPrice }) {
    return {
        titleValidation: validateName(title),
        descriptionValidation: validateDescription(description),
        priceValidation: validatePrice(mealPrice)
    }
}

export function satisfiesAllRegisterItemValidation({ titleValidation, descriptionValidation, priceValidation }) {
    return titleValidation.isValid && descriptionValidation.isValid && priceValidation.isValid
}
