import IconButton from '@material-ui/core/IconButton'
import ListIcon from '@material-ui/icons/List'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import PropTypes from 'prop-types'
import React from 'react'

import { revokeToken } from '../../lib/services/userService'
import { clearUserSession, getUserSession } from '../../lib/session'

const ITEM_HEIGHT = 48

export default function MenuButton({ router }) {
    const [anchorEl, setAnchorEl] = React.useState(null)
    const open = Boolean(anchorEl)

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget)
    }

    const handleClose = () => {
        setAnchorEl(null)
    }

    const handleCloseSesion = async () => {
        setAnchorEl(null)

        try {
            const { uid, jwt } = getUserSession()

            clearUserSession()
            await revokeToken(jwt, uid)
            router.push('/signin')
        } catch (error) {
            console.log(error)
        }
    }

    const handleProfile = () => {
        setAnchorEl(null)
        router.push('/profile')
    }

    return (
        <div>
            <IconButton aria-label="more" aria-controls="long-menu" aria-haspopup="true" onClick={handleClick}>
                <ListIcon />
            </IconButton>
            <Menu
                id="long-menu"
                anchorEl={anchorEl}
                keepMounted
                open={open}
                onClose={handleClose}
                PaperProps={{
                    style: {
                        maxHeight: ITEM_HEIGHT * 4.5,
                        width: '20ch'
                    }
                }}
            >
                <MenuItem onClick={handleProfile}>Perfil</MenuItem>
                <MenuItem onClick={handleCloseSesion}>Cerrar Sesión</MenuItem>
            </Menu>
        </div>
    )
}

MenuButton.propTypes = {
    router: PropTypes.object.isRequired
}
