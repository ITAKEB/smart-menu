import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { Button, CardActions, Container, Divider, Grid } from '@material-ui/core'

const useStyles = makeStyles({
    root: {
        maxWidth: '100%',
        marginTop: '2%'
    },
    media: {
        height: '400px'
    }
})

export default function RestaurantCard(props) {
    const {
        nombre,
        logo,
        email,
        country,
        city,
        address,
        phoneNumber,
        description,
        isAdmin,
        hasMenu,
        restaurantId,
        classes,
        onClick
    } = props

    const addMenuButton = (
        <Link href={'/restaurants/[restaurantId]/addMenu'} as={`/restaurants/${restaurantId}/addMenu`} passHref>
            <Button size="small" color="primary" variant="outlined">
                Añadir Menu
            </Button>
        </Link>
    )

    const viewMenuButton = (
        <Link
            href={{
                pathname: '/restaurants/[restaurantId]/menu',
                query: { mealTypeId: null }
            }}
            as={`/restaurants/${restaurantId}/menu`}
            passHref
        >
            <Button size="small" color="primary" variant="outlined">
                Ver Menu
            </Button>
        </Link>
    )

    return (
        <Card className={classes.cardRoot}>
            <CardActionArea onClick={onClick}>
                <CardMedia className={classes.media} src="Restaurant img" image={logo} title="Restaurante" />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {nombre}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {email}
                        <br />
                        {country}
                        <br />
                        {city}
                        <br />
                        {address}
                        <br />
                        {phoneNumber}
                        <br />
                        {description}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <Divider />
            <CardActions>
                <Grid container justifyContent="center">
                    {isAdmin && !hasMenu ? addMenuButton : null}
                    {hasMenu ? viewMenuButton : null}
                </Grid>
            </CardActions>
        </Card>
    )
}

RestaurantCard.propTypes = {
    nombre: PropTypes.string.isRequired,
    logo: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired,
    city: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    phoneNumber: PropTypes.string.isRequired,
    description: PropTypes.string,
    isAdmin: PropTypes.bool.isRequired,
    hasMenu: PropTypes.bool.isRequired,
    restaurantId: PropTypes.string.isRequired,
    classes: PropTypes.object.isRequired,
    onClick: PropTypes.func
}
