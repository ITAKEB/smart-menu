import Snackbar from '@material-ui/core/Snackbar'
import MuiAlert from '@material-ui/lab/Alert'

export function FailAlert({ showFail, onClose, text, duration }) {
    const alert = (
        <Snackbar open={showFail} autoHideDuration={duration} onClose={onClose}>
            <MuiAlert severity="error">{text}</MuiAlert>
        </Snackbar>
    )

    return showFail ? alert : null
}

export function SuccessAlert({ showSuccess, onClose, text, duration }) {
    const alert = (
        <Snackbar open={showSuccess} autoHideDuration={duration} onClose={onClose}>
            <MuiAlert severity="success">{text}</MuiAlert>
        </Snackbar>
    )

    return showSuccess ? alert : null
}
