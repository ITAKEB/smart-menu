import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import PropTypes from 'prop-types'

export function TextFieldError({ message }) {
    return (
        <Container component="div" maxWidth="xs">
            <Typography component="p" color="error" variant="subtitle1" align="center">
                {' '}
                {message}{' '}
            </Typography>
        </Container>
    )
}

TextFieldError.propTypes = {
    message: PropTypes.string.isRequired
}
