import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Grid from '@material-ui/core/Grid'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import React, { useState, useEffect } from 'react'
import Rating from '@material-ui/lab/Rating'

import Popup from './Popup'
import AddReview from '../../pages/addReview'
import Reviews from '../../pages/reviews'

const useStyles = makeStyles({
    root: {
        marginTop: 30
    }
})

export default function ItemCard({ title, description, price, id, score, imgURL }) {
    const classes = useStyles()
    const [state, setState] = useState({
        openPopup: false,
        openPopupAddReview: false
    })

    const handleClosedRating = () => {
        setState((prevSt) => ({ ...prevSt, openPopup: false }))
    }

    const handleOpenAddReview = () => {
        setState((prevSt) => ({
            ...prevSt,
            openPopupAddReview: !state.openPopupAddReview,
            openPopup: !state.openPopup
        }))
    }

    return (
        <Card className={classes.root}>
            <CardActionArea
                onClick={() => {
                    setState((prevSt) => ({ ...prevSt, openPopup: true }))
                }}
            >
                <Grid container spacing={3} alignItems="center" justifyContent="center">
                    <Grid item xs={12} sm={4}>
                        <CardMedia
                            component="img"
                            alt="restaurant item"
                            height="170"
                            image={imgURL}
                            title="restaurant item"
                        />
                    </Grid>
                    <Grid item xs={12} sm={8}>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {title}
                            </Typography>
                            <Rating
                                size="large"
                                name="score"
                                defaultValue={2.5}
                                precision={0.5}
                                value={parseInt(score.toFixed(1))}
                                readOnly
                            />
                            <Typography gutterBottom variant="h5" component="h3">
                                {`$${price}`}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                {description}
                            </Typography>
                        </CardContent>
                    </Grid>
                </Grid>
            </CardActionArea>
            <CardActions>
                <Button
                    onClick={() => {
                        setState((prevSt) => ({ ...prevSt, openPopup: true }))
                    }}
                    size="small"
                    color="primary"
                >
                    Ratings
                </Button>
            </CardActions>
            <Popup title={title} openPopup={state.openPopup} setOpenPopup={handleClosedRating}>
                <Reviews itemId={id} closedPopup={handleOpenAddReview} />
            </Popup>
            <Popup title={title} openPopup={state.openPopupAddReview} setOpenPopup={handleOpenAddReview}>
                <AddReview itemId={id} closedPopup={handleOpenAddReview} />
            </Popup>
        </Card>
    )
}

ItemCard.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    id: PropTypes.string.isRequired,
    score: PropTypes.number.isRequired,
    imgURL: PropTypes.string.isRequired
}
