import AppBar from '@material-ui/core/AppBar'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import Link from '@material-ui/core/Link'
import LinkN from 'next/link'
import PropTypes from 'prop-types'
import React, { useState, useEffect } from 'react'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import { useRouter } from 'next/router'

import { getUserSession } from '../../lib/session'
import MenuButton from './MenuButton'
import { createTheme } from '@material-ui/core/styles'
import { ThemeProvider } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                SmartMenu
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    )
}

const useStyles = makeStyles((theme) => ({
    '@global': {
        ul: {
            margin: 0,
            padding: 0,
            listStyle: 'none'
        }
    },
    appBar: {
        borderBottom: `1px solid ${theme.palette.divider}`
    },
    toolbar: {
        flexWrap: 'wrap'
    },
    toolbarTitle: {
        flexGrow: 1,
        fontFamily: 'Sacramento, Arial',
        fontSize: '2em'
    },
    link: {
        margin: theme.spacing(1, 1.5),
        fontSize: '1em'
    },
    footer: {
        borderTop: `1px solid ${theme.palette.divider}`,
        marginTop: theme.spacing(8),
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(3),
        [theme.breakpoints.up('sm')]: {
            paddingTop: theme.spacing(6),
            paddingBottom: theme.spacing(6)
        }
    }
}))

export default function Layout({ children }) {
    const classes = useStyles()
    const [state, setState] = useState({ hasSession: false })
    const router = useRouter()

    useEffect(() => {
        getUserSession() == null ? setState({ hasSession: false }) : setState({ hasSession: true })
    }, [])

    return (
        <div>
            <CssBaseline />
            <AppBar position="static" color="default" elevation={0} className={classes.appBar}>
                <Toolbar className={classes.toolbar}>
                    {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
                    <CssBaseline />
                    <Typography variant="h6" color="inherit" noWrap className={classes.toolbarTitle}>
                        <Link color="textPrimary" href="/">
                            SmartMenu
                        </Link>
                    </Typography>

                    <Link variant="button" color="textPrimary" href="/about" className={classes.link}>
                        Sobre nosotros
                    </Link>
                    {state.hasSession ? (
                        <MenuButton router={router} className={classes.link} />
                    ) : (
                        <LinkN href="/signin">
                            <Button color="primary" variant="outlined" className={classes.link}>
                                Iniciar sesión
                            </Button>
                        </LinkN>
                    )}
                </Toolbar>
            </AppBar>
            {children}
            <Container maxWidth="md" component="footer" className={classes.footer}>
                <Box mt={5}>
                    <Copyright />
                </Box>
            </Container>
        </div>
    )
}

Layout.propTypes = {
    children: PropTypes.node
}
