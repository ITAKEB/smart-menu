import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Grid from '@material-ui/core/Grid'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'
import Rating from '@material-ui/lab/Rating'
import FavoriteIcon from '@material-ui/icons/Favorite'
import { makeStyles } from '@material-ui/core/styles'
import React, { useState, useEffect } from 'react'
import { getUserSession } from '../../lib/session'
import { getLikes, postLike } from '../../lib/services/reviewService'

const useStyles = makeStyles({
    root: {
        marginTop: 30
    },
    prueba: {
        backgroundColor: 'red'
    },
    likes: {
        display: 'flex',
        alignItems: 'center',
        alignContent: 'center'
    },
    header: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    pointer: {
        cursor: 'pointer'
    }
})

export default function Review({ user, review, rating, likes, category, id }) {
    const classes = useStyles()
    const [state, setState] = useState({
        likes: 0
    })

    useEffect(() => {
        async function fetchData() {
            const likes = await getLikes(id)

            setState((prevSt) => ({ ...prevSt, likes }))
        }

        fetchData()
    }, [id])

    const onClick = async () => {
        const { jwt, uid } = getUserSession()
        //let likes = state.likes
        try {
            const bool = await postLike(jwt, uid, id)

            const likes = await getLikes(id)
            //console.log(bool)
            //console.log(likes)
            setState((prevSt) => ({ ...prevSt, likes }))
        } catch (error) {
            if (error == 'LikeAlreadyInserted') {
                console.log('Like was inserted')
            }
        }
    }

    return (
        <Card className={classes.root}>
            <CardActions>
                <Grid container spacing={3} alignItems="center" justifyContent="center">
                    <Grid item xs={12}>
                        <CardContent>
                            <Typography className={classes.header} gutterBottom variant="h6">
                                {user}
                            </Typography>

                            <Rating
                                size="medium"
                                readOnly
                                name="rating"
                                defaultValue={2.5}
                                precision={0.5}
                                value={parseFloat(rating)}
                            />

                            <Typography gutterBottom variant="body2">
                                {review}
                            </Typography>

                            <Typography className={classes.likes} gutterBottom variant="body2">
                                <FavoriteIcon onClick={onClick} className={classes.pointer} /> {state.likes}
                            </Typography>
                        </CardContent>
                    </Grid>
                </Grid>
            </CardActions>
        </Card>
    )
}

Review.propTypes = {
    user: PropTypes.string.isRequired,
    review: PropTypes.string,
    rating: PropTypes.number.isRequired,
    likes: PropTypes.number.isRequired,
    category: PropTypes.string,
    id: PropTypes.string
}
