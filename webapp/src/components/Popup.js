import React from 'react'
import { Dialog, DialogTitle, DialogContent, makeStyles, Typography } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import PropTypes from 'prop-types'

const useStyles = makeStyles((theme) => ({
    dialogWrapper: {
        position: 'absolute',
        top: theme.spacing(0),
        width: '100%'
    },
    header: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%'
    }
}))

export default function Popup({ title, children, openPopup, setOpenPopup }) {
    const classes = useStyles()

    const handleOnClick = () => {
        setOpenPopup()
    }

    return (
        <Dialog open={openPopup} maxWidth="md" classes={{ paper: classes.dialogWrapper }}>
            <DialogTitle>
                <div className={classes.header}>
                    <Typography variant="h6" component="div">
                        {title}
                    </Typography>
                    <Button onClick={handleOnClick} variant="outlined" color="primary">
                        X
                    </Button>
                </div>
            </DialogTitle>
            <DialogContent dividers>{children}</DialogContent>
        </Dialog>
    )
}

Popup.propTypes = {
    title: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
    openPopup: PropTypes.bool.isRequired,
    setOpenPopup: PropTypes.func.isRequired
}
