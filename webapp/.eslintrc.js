module.exports = {
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true
        }
    },
    env: {
        browser: true,
        node: true,
        es6: true
    },
    settings: {
        react: {
            version: 'detect'
        }
    },
    extends: ['next', 'plugin:react/recommended', 'prettier'],
    plugins: ['react', 'prettier'],
    rules: {
        'react/react-in-jsx-scope': 'off'
    }
}
