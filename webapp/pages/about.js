import { Container, Typography } from '@material-ui/core'
import Link from 'next/link'
import PropTypes from 'prop-types'
import React, { useState, useEffect } from 'react'

import Layout from '../src/components/Layout'

export default function About() {
    return (
        <Layout>
            <Container maxWidth="md">
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <Typography component="h1" variant="h2" align="center" gutterBottom>
                    Sobre Nosotros
                </Typography>
                <Typography component="h1" variant="body2" align="justify" gutterBottom>
                    <p>
                        SmartMenu es una plataforma Web que permite a los clientes de restaurante encontrar información
                        de menús que se ajuste a sus necesidades en diferentes categorías, a saber,
                    </p>
                    <ul>
                        <li>
                            <i>Calidad:</i> Se refiere a que tan satisfecho esta un cliente con la calidad de un ítem
                            consumido.
                        </li>
                        <li>
                            <i>Precio:</i> Se refiere a si el cliente considera que el precio del ítem es razonable.
                        </li>
                        <li>
                            <i>Salud:</i> Se refiere a la percepción y conocimiento que el cliente tiene sobre la
                            calidad nutricional del ítem de menú.
                        </li>
                        <li>
                            <i>Ecología:</i> Se refiere a la percepcion y conocimiento que el cliente tiene sobre las
                            práctica ecológicas en la elaboración del ítem de menú.
                        </li>
                    </ul>
                    <p>
                        De este modo, nuestro servicio da a los clientes de restaurante acceso a información que les
                        permite optimizar sus decisiones en el momento de elegir un ítem de un menú y, por otro lado, le
                        ayuda a los administradores de establecimientos a visualizar las puntuaciones y
                        retroalimentación que sus clientes tienen de los menús de comida.
                    </p>
                </Typography>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </Container>
        </Layout>
    )
}
