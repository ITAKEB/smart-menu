import Button from '@material-ui/core/Button'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import StarBorderIcon from '@material-ui/icons/StarBorder'
import AddIcon from '@material-ui/icons/Add'
import { makeStyles } from '@material-ui/core'
import React, { useState, useEffect } from 'react'

import { getUserSession } from '../lib/session'
import { getAvarageRatings, getReviews } from '../lib/services/reviewService'
import Review from '../src/components/Review'

const useStyles = makeStyles((theme) => ({
    prueba: {
        backgroundColor: 'red'
    },
    starIcon: {
        color: 'rgb(251, 186, 0)'
    }
}))

export default function Reviews({ itemId, closedPopup }) {
    const classes = useStyles()

    const [state, setState] = useState({
        openPopup2: false,
        hasSession: false,
        healthiness: 0,
        pricefairness: 0,
        quality: 0,
        ecology: 0,
        category: 'Quality',
        currentSelected: 'Quality',
        reviews: null
    })

    const closedBeforePopup = () => {
        closedPopup()
    }

    useEffect(() => {
        async function fetchData() {
            const hasSession = getUserSession() ? true : false
            const reviews = await getReviews(itemId, state.category, 0, 10)

            setState((prevSt) => ({ ...prevSt, hasSession, reviews }))

            const avarageRatings = await getAvarageRatings(itemId)
            avarageRatings.map((v) => {
                const key = v.category.toLowerCase()
                const avg = v.average == 0 ? v.average : v.average.toFixed(1)
                setState((prevSt) => ({ ...prevSt, [key]: avg }))
            })
        }

        fetchData()
    }, [itemId, state.category])

    const onClickCat = (category) => {
        return async (evt) => {
            evt.preventDefault()
            const reviews = await getReviews(itemId, category, 0, 10)

            setState((prevSt) => ({ ...prevSt, category, reviews, currentSelected: category }))
        }
    }

    const categories = [
        {
            onClick: onClickCat('Quality'),
            title: 'Calidad',
            disabled: state.currentSelected === 'Quality',
            avg: state.quality
        },
        {
            onClick: onClickCat('Healthiness'),
            title: 'Salud',
            disabled: state.currentSelected === 'Healthiness',
            avg: state.healthiness
        },
        {
            onClick: onClickCat('PriceFairness'),
            title: 'Economía',
            disabled: state.currentSelected === 'PriceFairness',
            avg: state.pricefairness
        },
        {
            onClick: onClickCat('Ecology'),
            title: 'Ecología',
            disabled: state.currentSelected === 'Ecology',
            avg: state.ecology
        }
    ]

    return (
        <div>
            <Grid container>
                <Grid container item xs={12}>
                    <Grid container item xs={10}>
                        <Grid container item justifyContent="flex-start" spacing={1} xs={11}>
                            {categories.map(({ onClick, title, disabled, avg }) => (
                                <CategoryButton
                                    key={title}
                                    title={title}
                                    onClick={onClick}
                                    disabled={disabled}
                                    classes={classes}
                                    avg={parseFloat(avg)}
                                />
                            ))}
                        </Grid>
                    </Grid>
                    <Grid container item justifyContent="flex-end" xs={2}>
                        <Grid item xs="auto">
                            <ReviewButton onClick={closedBeforePopup} hasSession={state.hasSession} />
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item container xs={12}>
                    <ReviewsList reviews={state.reviews} />
                </Grid>
            </Grid>
        </div>
    )
}

function ReviewButton({ hasSession, onClick }) {
    const button = (
        <Button onClick={onClick} size="small" variant="outlined" color="primary">
            <AddIcon />
            Hacer reseña
        </Button>
    )

    return hasSession ? button : null
}

function ReviewsList({ reviews }) {
    if (reviews === null || reviews.length < 1) {
        return null
    }

    const component = reviews.map((r) => {
        return (
            <Grid key={r.idx} item xs={12}>
                <Review id={r.idx} user={r.userName} review={r.content} rating={r.score} likes={r.likes} />
            </Grid>
        )
    })

    return component
}

function CategoryButton({ classes, onClick, disabled, title, avg }) {
    return (
        <Grid item xs="auto">
            <Button onClick={onClick} size="small" variant="outlined" color="default" disabled={disabled}>
                {title}
                <StarBorderIcon className={classes.starIcon} />
                {avg}
            </Button>
        </Grid>
    )
}

Reviews.propTypes = {
    itemId: PropTypes.string.isRequired,
    closedPopup: PropTypes.func.isRequired
}

ReviewButton.propTypes = {
    hasSession: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired
}

CategoryButton.propTypes = {
    classes: PropTypes.object.isRequired,
    onClick: PropTypes.func.isRequired,
    disabled: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    avg: PropTypes.number.isRequired
}

ReviewsList.propTypes = {
    reviews: PropTypes.array
}
