import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core'
import Rating from '@material-ui/lab/Rating'
import React, { useState, useEffect } from 'react'
import InfoIcon from '@material-ui/icons/Info'
import PropTypes from 'prop-types'

import { getUserSession } from '../lib/session'
import { postReview, mkReview } from '../lib/services/reviewService'
import { FailAlert, SuccessAlert } from '../src/components/Alert'
import { validateReview } from '../lib/validator'

const useStyles = makeStyles((theme) => ({
    reviewField: {
        width: '100%',
        marginBottom: '10px'
    },
    form: {
        width: '100%'
    },
    info: {
        display: 'flex',
        padding: '1%',
        alignItems: 'center'
    },
    infoText: {
        marginRight: '10px'
    }
}))

export default function AddReview({ itemId, closedPopup }) {
    const classes = useStyles()
    const [state, setState] = useState({
        session: { email: '' },
        category: '',
        score: 0,
        currentSelected: '',
        content: '',
        showSuccess: false,
        showFail: false,
        error: null
    })

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()

            setState((prevSt) => ({ ...prevSt, [attr]: evt.target.value }))
        }
    }

    const handleSubmit = async (evt) => {
        evt.preventDefault()
        let session = state.session
        const cnt = state.content.length < 1 ? null : state.content
        let reviewData = mkReview(state.category, cnt, parseFloat(state.score))
        const validator = validateReview(cnt || '')

        if (state.currentSelected.length < 1) {
            setState((prevSt) => ({ ...prevSt, error: 'Debes seleccionar una categoría', showFail: true }))
            return
        }

        if (!validator.isValid) {
            setState((prevSt) => ({ ...prevSt, error: validator.message, showFail: true }))
            return
        }

        try {
            await postReview(session.jwt, session.uid, itemId, reviewData)
            setState((prevSt) => ({ ...prevSt, content: '', score: 0, showSuccess: true }))
        } catch (error) {
            console.log(error)
            if (error === 'NotAllowedToReview') {
                setState((prevSt) => ({ ...prevSt, error: 'Has publicado en menos de 24h', showFail: true }))
            }
        }
    }

    const handleCancel = () => {
        closedPopup()
    }

    const onClickCat = (category) => {
        return (evt) => {
            evt.preventDefault()
            setState((prevSt) => ({ ...prevSt, category, currentSelected: category }))
        }
    }

    const onCloseSuccessAlert = () => {
        setState((prevSt) => ({ ...prevSt, showSuccess: false }))
    }

    const onCloseFailAlert = () => {
        setState((prevSt) => ({ ...prevSt, showFail: false }))
    }

    useEffect(() => {
        const session = getUserSession()
        setState((prevSt) => ({ ...prevSt, session }))
    }, [])

    const categories = [
        { onClick: onClickCat('Quality'), title: 'Calidad', disabled: state.currentSelected === 'Quality' },
        { onClick: onClickCat('Healthiness'), title: 'Salud', disabled: state.currentSelected === 'Healthiness' },
        {
            onClick: onClickCat('PriceFairness'),
            title: 'Economía',
            disabled: state.currentSelected === 'PriceFairness'
        },
        { onClick: onClickCat('Ecology'), title: 'Ecología', disabled: state.currentSelected === 'Ecology' }
    ]

    return (
        <div>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Typography variant="h6">{state.session.email}</Typography>
                    <div className={classes.info}>
                        <Typography className={classes.infoText} variant="body2">
                            Se mostrará publicamente
                        </Typography>
                        <InfoIcon />
                    </div>
                </Grid>

                <Grid item container spacing={2} xs={12}>
                    {categories.map(({ onClick, title, disabled }) => (
                        <CategoryButton key={title} title={title} onClick={onClick} disabled={disabled} />
                    ))}
                </Grid>
                <Grid item xs={12}>
                    <Rating
                        size="large"
                        name="score"
                        defaultValue={2.5}
                        precision={0.5}
                        value={parseFloat(state.score)}
                        onChange={handleInputChange('score')}
                    />
                </Grid>
                <Grid item container xs={12}>
                    <form className={classes.form} onSubmit={handleSubmit}>
                        <Grid item xs={12}>
                            <TextField
                                id="content"
                                label="Reseña"
                                multiline
                                rows={4}
                                placeholder="Inserte aquí su opinión"
                                className={classes.reviewField}
                                variant="outlined"
                                value={state.content}
                                onChange={handleInputChange('content')}
                            />
                        </Grid>
                        <Grid item align="right" xs={12}>
                            <Button onClick={handleCancel} size="small" variant="outlined" color="primary">
                                Cancelar
                            </Button>
                            <Button type="submit" size="small" variant="outlined" color="primary">
                                Publicar
                            </Button>
                        </Grid>
                    </form>
                    <Grid item container xs={12}>
                        <SuccessAlert
                            text="Reseña publicada"
                            duration={4000}
                            showSuccess={state.showSuccess}
                            onClose={onCloseSuccessAlert}
                        />
                        <FailAlert
                            text={state.error}
                            duration={4000}
                            showFail={state.showFail}
                            onClose={onCloseFailAlert}
                        />
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}

function CategoryButton({ onClick, disabled, title }) {
    return (
        <Grid item>
            <Button onClick={onClick} size="small" variant="outlined" color="default" disabled={disabled}>
                {title}
            </Button>
        </Grid>
    )
}

FailAlert.prototypes = {
    showFail: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired
}

SuccessAlert.prototypes = {
    showSuccess: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired
}

AddReview.propTypes = {
    itemId: PropTypes.string.isRequired,
    closedPopup: PropTypes.func.isRequired
}

CategoryButton.propTypes = {
    title: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    disabled: PropTypes.bool.isRequired
}
