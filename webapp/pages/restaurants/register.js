import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import CssBaseline from '@material-ui/core/CssBaseline'
import Grid from '@material-ui/core/Grid'
import RestaurantIcon from '@material-ui/icons/Restaurant'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import { useRouter } from 'next/router'
import { useState, React } from 'react'
import Fab from '@material-ui/core/Fab'
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate'

import { FailAlert } from '../../src/components/Alert'
import { TextFieldError } from '../../src/components/InputError'
import { mkRestaurantData, postRestaurantImage, postRestaurant } from '../../lib/services/restaurantService.js'
import { getUserSession } from '../../lib/session'
import Layout from '../../src/components/Layout.js'
import { registerRestaurantValidator, satisfiesAllRegisterRestaurantValidation } from '../../lib/validator'

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.primary.main
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    },
    input: {
        display: 'none'
    }
}))

export default function Register() {
    const router = useRouter()
    const classes = useStyles()
    const [state, setState] = useState({
        name: '',
        email: '',
        country: '',
        city: '',
        address: '',
        phoneNumber: '',
        description: '',
        file: null,
        validations: null,
        submitDisabled: true,
        showFail: false,
        error: null
    })

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()

            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: evt.target.value.trim() }
                newState.validations = registerRestaurantValidator(newState)

                if (satisfiesAllRegisterRestaurantValidation(newState.validations) && state.file) {
                    newState.submitDisabled = false
                } else {
                    newState.submitDisabled = true
                }

                return newState
            }

            setState(updateState)
        }
    }

    const handleSubmit = async (evt) => {
        evt.preventDefault()

        if (state.validations && satisfiesAllRegisterRestaurantValidation(state.validations)) {
            try {
                const { jwt, uid } = getUserSession()
                const { email, name, country, city, address, phoneNumber, description } = state
                const restaurantData = mkRestaurantData(email, name, country, city, address, phoneNumber, description)

                const restaurant = await postRestaurant(jwt, uid, restaurantData)
                await postRestaurantImage(jwt, uid, restaurant.idx, state.file)

                router.push('/profile')
            } catch (error) {
                console.log(error)
            }
        }
    }

    const handleUploadClick = async (event) => {
        let file = event.target.files[0]

        if (!file) {
            return
        }

        const { type, size } = file

        const maxImgSize = 800000 // 800KBs

        if (size > maxImgSize) {
            setState((prevSt) => ({ ...prevSt, showFail: true, error: 'La imagen debe ser menor o igual a 800KBs' }))
        }

        if (type == 'image/jpeg') {
            //await postImage(jwt, uid, 'df68a7d9-de4d-4d6e-b9e0-30e0d44b103e', file)
            let submitDisabled = true
            if (state.validations && satisfiesAllRegisterRestaurantValidation(state.validations)) {
                submitDisabled = false
            }
            setState((prevSt) => ({ ...prevSt, file, submitDisabled }))
        }
    }

    const onCloseFailAlert = () => {
        setState((prevSt) => ({ ...prevSt, showFail: false }))
    }

    return (
        <Layout>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <RestaurantIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Información de restaurante
                    </Typography>
                    <form className={classes.form} noValidate onSubmit={handleSubmit}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    name="email"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Correo electronico"
                                    onChange={handleInputChange('email')}
                                />
                                <FieldError validations={state.validations} validationType="emailValidation" />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="name"
                                    label="Nombre"
                                    name="name"
                                    onChange={handleInputChange('name')}
                                />
                                <FieldError validations={state.validations} validationType="nameValidation" />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="country"
                                    label="Pais"
                                    onChange={handleInputChange('country')}
                                />
                                <FieldError validations={state.validations} validationType="countryValidation" />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="city"
                                    label="Ciudad"
                                    onChange={handleInputChange('city')}
                                />
                                <FieldError validations={state.validations} validationType="cityValidation" />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="address"
                                    label="Dirección"
                                    onChange={handleInputChange('address')}
                                />
                                <FieldError validations={state.validations} validationType="addressValidation" />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="phoneNumber"
                                    label="Numero de telefono"
                                    onChange={handleInputChange('phoneNumber')}
                                />
                                <FieldError validations={state.validations} validationType="phoneNumberValidation" />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    multiline
                                    name="description"
                                    label="Descripción"
                                    onChange={handleInputChange('description')}
                                />
                                <FieldError validations={state.validations} validationType="descriptionValidation" />
                            </Grid>
                            <Grid item xs={12}>
                                <input
                                    accept="image/jpeg"
                                    className={classes.input}
                                    id="contained-button-file"
                                    multiple
                                    type="file"
                                    onChange={handleUploadClick}
                                />
                                <label htmlFor="contained-button-file">
                                    <Fab component="span" className={classes.button}>
                                        <AddPhotoAlternateIcon />
                                    </Fab>
                                </label>
                            </Grid>
                        </Grid>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            disabled={state.submitDisabled}
                        >
                            Registrar
                        </Button>
                    </form>
                    <FailAlert
                        text={state.error}
                        duration={4000}
                        showFail={state.showFail}
                        onClose={onCloseFailAlert}
                    />
                </div>
            </Container>
        </Layout>
    )
}

function FieldError({ validations, validationType }) {
    const fieldError =
        !validations || validations[validationType].isValid ? null : (
            <TextFieldError message={validations[validationType].message} />
        )
    return fieldError
}
