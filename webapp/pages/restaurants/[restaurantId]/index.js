import Link from 'next/link'
import React, { useState, useEffect } from 'react'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

import Layout from '../../../src/components/Layout'
import RestaurantCard from '../../../src/components/RestaurantCard'
import { getUserSession } from '../../../lib/session'
import { getRestaurantById, getHasMenu } from '../../../lib/services/restaurantService'
import { getImgUrl } from '../../../lib/services/imageService'

const useStyles = makeStyles({
    cardRoot: {
        maxWidth: '100%',
        marginTop: '2%'
    },
    media: {
        height: '400px'
    }
})

export default function Restaurant({ restaurantId }) {
    const classes = useStyles()

    const [state, setState] = useState({
        name: '',
        country: '',
        city: '',
        email: '',
        address: '',
        phoneNumber: '',
        description: '',
        adminId: '',
        imgURL: '',
        hasMenu: false
    })

    const isAdmin = () => {
        const session = getUserSession()
        return session && session.uid === state.adminId
    }

    useEffect(() => {
        const fetchRestaurant = async () => {
            const restaurant = await getRestaurantById(restaurantId)
            const imgURL = getImgUrl(restaurant.imageId, 2000, 1000)
            const hasMenu = await getHasMenu(restaurantId)

            setState({ ...restaurant, hasMenu, imgURL })
        }

        fetchRestaurant()
    }, [restaurantId])

    return (
        <Layout>
            <RestaurantCard
                classes={classes}
                nombre={state.name}
                logo={state.imgURL}
                restaurantId={restaurantId}
                email={state.email}
                country={state.country}
                city={state.city}
                address={state.address}
                phoneNumber={state.phoneNumber}
                description={state.description}
                isAdmin={isAdmin()}
                hasMenu={state.hasMenu}
            />
        </Layout>
    )
}

Restaurant.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId } = query

    return { restaurantId }
}

Restaurant.propTypes = {
    restaurantId: PropTypes.string.isRequired
}
