import AssistantPhotoIcon from '@material-ui/icons/AssistantPhoto'
import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import CssBaseline from '@material-ui/core/CssBaseline'
import FormControl from '@material-ui/core/FormControl'
import Grid from '@material-ui/core/Grid'
import InputLabel from '@material-ui/core/InputLabel'
import PropTypes from 'prop-types'
import Select from '@material-ui/core/Select'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import { useRouter } from 'next/router'
import { useState } from 'react'

import Layout from '../../../src/components/Layout.js'
import { mkMenuData, postMenu } from '../../../lib/services/restaurantService.js'
import { getUserSession } from '../../../lib/session'

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.success.light
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 220
    },
    selectEmpty: {
        marginTop: theme.spacing(2)
    }
}))

export default function AddMenu({ restaurantId }) {
    const router = useRouter()
    const classes = useStyles()
    const [state, setState] = useState({
        id: '',
        title: '',
        mealTypes: {},
        numMealTypes: 1,
        submitDisabled: true
    })

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()

            const v = evt.target.value
            const value = attr === 'numMealTypes' ? parseInt(v) : v
            const submitDisabled = typeof value === 'number'

            setState((prevSt) => ({ ...prevSt, submitDisabled, [attr]: value }))
        }
    }

    const handleMealFieldChange = (fieldId) => {
        return (evt) => {
            evt.preventDefault()
            const value = evt.target.value
            const updateState = (prevSt) => {
                let newMealTypes = {}
                const prevMealTypes = prevSt.mealTypes

                if (prevSt.mealTypes && value === '') {
                    const keys = Object.keys(prevMealTypes)
                    keys.map((k) => {
                        if (k != fieldId) {
                            newMealTypes[k] = prevMealTypes[k]
                        }
                    })
                } else {
                    newMealTypes = { ...prevMealTypes, [fieldId]: value }
                }

                const submitDisabled = !(prevSt.numMealTypes === Object.keys(newMealTypes).length)

                return { ...prevSt, submitDisabled, mealTypes: newMealTypes }
            }

            setState(updateState)
        }
    }

    const handleSubmit = async (evt) => {
        evt.preventDefault()
        try {
            const mealTypes = Object.values(state.mealTypes)
            const { jwt, uid } = getUserSession()
            const { title } = state
            const menuData = mkMenuData(title, mealTypes)

            await postMenu(jwt, uid, restaurantId, menuData)

            router.push(`/restaurants/${restaurantId}`)
        } catch (error) {
            console.log(error)
        }
    }

    const fieldIds = Array.from(Array(state.numMealTypes).keys())
    const mealTypeFields = fieldIds.map((i) => (
        <MealTypeField key={i + 1} fieldId={i + 1} onChange={handleMealFieldChange} />
    ))

    return (
        <Layout>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <AssistantPhotoIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Añadir Menu
                    </Typography>
                    <form className={classes.form} noValidate onSubmit={handleSubmit}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    name="title"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="title"
                                    label="Titulo"
                                    onChange={handleInputChange('title')}
                                />
                            </Grid>
                            <Grid item xs={12} sm={4}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel htmlFor="age-native-simple">Numero de categorias</InputLabel>
                                    <Select
                                        native
                                        value={state.numMealTypes}
                                        onChange={handleInputChange('numMealTypes')}
                                    >
                                        <MealOptions numOpts={10} />
                                    </Select>
                                </FormControl>
                            </Grid>
                            {mealTypeFields}
                        </Grid>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            disabled={state.submitDisabled}
                        >
                            Añadir Menu
                        </Button>
                    </form>
                </div>
            </Container>
        </Layout>
    )
}

function MealTypeField({ fieldId, onChange }) {
    return (
        <Grid item xs={12}>
            <TextField
                name={'mealType-' + fieldId}
                variant="outlined"
                required
                fullWidth
                id={fieldId.toString()}
                label={`Tipo de comida #${fieldId}`}
                onChange={onChange(fieldId)}
            />
        </Grid>
    )
}

function MealOptions({ numOpts }) {
    const optsIds = Array.from(Array(numOpts).keys())
    return optsIds.map((i) => (
        <option key={i + 1} value={i + 1}>
            {i + 1}
        </option>
    ))
}

AddMenu.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId } = query

    return { restaurantId }
}

AddMenu.propTypes = {
    restaurantId: PropTypes.string.isRequired
}

MealTypeField.propTypes = {
    fieldId: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
}

MealOptions.propTypes = {
    restaurants: PropTypes.arrayOf(PropTypes.number)
}
