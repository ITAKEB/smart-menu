import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import PropTypes from 'prop-types'
import React, { useState, useEffect } from 'react'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import Link from 'next/link'
import Rating from '@material-ui/lab/Rating'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import RefreshIcon from '@material-ui/icons/Refresh'
import { Button } from '@material-ui/core'

import ItemCard from '../../../src/components/ItemCard'
import Layout from '../../../src/components/Layout'
import {
    getItemsByCategoriesScore,
    getItemsByGlobalScore,
    getMenuWithItems,
    getRestaurantById
} from '../../../lib/services/restaurantService'
import { getUserSession } from '../../../lib/session'
import { getImgUrl } from '../../../lib/services/imageService'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        width: '100%',
        margin: 'auto'
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary
    },
    toolbar: {
        marginLeft: '25%',
        marginTop: '10%'
    },
    buttons: {
        marginBottom: '5%'
    },
    refreshIcon: {
        marginLeft: '20px'
    }
}))

export default function Menu({ restaurantId, mealTypeId }) {
    const classes = useStyles()
    const [state, setState] = useState({
        menu: null,
        items: null,
        adminId: '',
        isAdmin: false,
        currentMealTypeId: undefined,
        query: undefined,
        filters: {
            General: { name: 'General', score: 0 },
            Quality: { name: 'Calidad', score: 0 },
            Healthiness: { name: 'Salud', score: 0 },
            PriceFairness: { name: 'Economia', score: 0 },
            Ecology: { name: 'Ecologia', score: 0 }
        }
    })

    useEffect(() => {
        const fetchMenu = async () => {
            const { menu, items } = await getMenuWithItems(restaurantId)
            const mealTypeIds = menu.mealTypes.map((mt) => {
                return mt.idx
            })
            let currentMealTypeId
            if (!mealTypeId) {
                currentMealTypeId = mealTypeIds.length > 0 ? mealTypeIds[0] : undefined
            } else {
                currentMealTypeId = mealTypeId
            }
            const session = getUserSession()
            const restaurant = await getRestaurantById(restaurantId)
            const isAdmin = session ? restaurant.adminId === session.uid : false

            setState((prevSt) => ({ ...prevSt, menu, items, currentMealTypeId, isAdmin }))
        }

        fetchMenu()
    }, [restaurantId, mealTypeId])

    const onClick = (idx) => {
        return () => {
            let updatedFilters = state.filters
            filterTypes.map((k) => {
                updatedFilters[k].score = 0
            })
            setState((prevSt) => ({ ...prevSt, currentMealTypeId: idx, filters: updatedFilters }))
        }
    }

    const filterTypes = ['General', 'Quality', 'Healthiness', 'PriceFairness', 'Ecology']

    const handleOnChange = (label) => {
        return (evt) => {
            const value = evt.target.value
            const count = state.count + 1
            let query = undefined

            setState((prevSt) => {
                const updatedFilters = { ...prevSt.filters }
                updatedFilters[label].score = value

                if (label === 'General') {
                    query = async (mealTypeId) => await getItemsByGlobalScore(mealTypeId, value)
                    filterTypes.map((k) => {
                        if (k != 'General') {
                            updatedFilters[k].score = 0
                        }
                    })
                } else {
                    updatedFilters['General'].score = 0
                    let categories = []
                    let values = []
                    filterTypes.map((k) => {
                        if (updatedFilters[k].score != 0) {
                            categories.push(k)
                            values.push(parseInt(updatedFilters[k].score))
                        }
                    })
                    query = async (mealTypeId) =>
                        await getItemsByCategoriesScore(
                            mealTypeId,
                            `[${categories.toString()}]`,
                            `[${values.toString()}]`
                        )
                }

                return { ...prevSt, query, count, filters: updatedFilters }
            })
        }
    }

    const handleReset = (label) => {
        const filters = state.filters
        let categories = []
        let values = []
        let query = undefined
        filterTypes.map((k) => {
            if (filters[k].score != 0 && k != label) {
                categories.push(k)
                values.push(parseInt(filters[k].score))
            }
        })
        query = async (mealTypeId) =>
            await getItemsByCategoriesScore(mealTypeId, `[${categories.toString()}]`, `[${values.toString()}]`)

        return (evt) => {
            evt.preventDefault()
            filters[label].score = 0
            setState((prevSt) => ({ ...prevSt, filters, query }))
            handleOnChange(label)
        }
    }

    const handleRefreshSearch = async (evt) => {
        evt.preventDefault()

        const accum = Object.values(state.filters).reduce((o0, o1) => {
            const score = parseInt(o0.score) + parseInt(o1.score)
            return { score }
        })

        if (accum.score === 0) {
            const { items } = await getMenuWithItems(restaurantId)

            setState((prevSt) => ({ ...prevSt, items }))
        } else {
            const itemsResponse = await state.query(state.currentMealTypeId)
            const items = state.items
            const currentMealTypeId = state.currentMealTypeId
            items[currentMealTypeId] = itemsResponse
            setState((prevSt) => ({ ...prevSt, items }))
        }
    }

    const addItemButton = (
        <Link href={'/restaurants/[restaurantId]/addItem'} as={`/restaurants/${restaurantId}/addItem`} passHref>
            <Button variant="outlined">Añadir item</Button>
        </Link>
    )

    return (
        <Layout>
            <div className={classes.root}>
                <Grid container spacing={3} justifyContent="center">
                    <Grid item xs={12} sm={4}>
                        <div className={classes.toolbar}>
                            <Typography component="h1" variant="h4" align="left" gutterBottom>
                                {state.menu ? state.menu.title : ''}
                            </Typography>
                            <Typography component="h4" variant="h5" align="left" gutterBottom>
                                Categorias
                                <Button
                                    className={classes.refreshIcon}
                                    onClick={handleRefreshSearch}
                                    variant="outlined"
                                >
                                    <RefreshIcon />
                                </Button>
                            </Typography>

                            <MealTypes
                                currentMealTypeId={state.currentMealTypeId}
                                menu={state.menu}
                                onClick={onClick}
                            />
                            <Divider />
                            <Typography component="h4" variant="h5" align="left" gutterBottom>
                                Filtros
                            </Typography>
                            <List>
                                <div className={classes.buttons}>
                                    <Filters
                                        onReset={handleReset}
                                        filters={state.filters}
                                        filterTypes={filterTypes}
                                        classes={classes}
                                        onChange={handleOnChange}
                                    />
                                </div>
                                <div>{state.isAdmin ? addItemButton : null}</div>
                            </List>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={8}>
                        <CurrentItems mealTypeId={state.currentMealTypeId} items={state.items} />
                    </Grid>
                </Grid>
            </div>
        </Layout>
    )
}

function Filters({ filters, onChange, filterTypes, onReset }) {
    const components = filterTypes.map((label, i) => {
        const filter = filters[label]
        return (
            <Filter
                i={i}
                key={i}
                name={filter.name}
                label={label}
                score={parseFloat(filter.score)}
                onReset={onReset}
                onChange={onChange}
            />
        )
    })

    return components
}

function Filter({ i, label, score, onChange, onReset, name }) {
    return (
        <div key={i}>
            <ListItem key={i}>
                <ListItemText primary={name} />
                {score !== 0 ? (
                    <Button onClick={onReset(label)} variant="outlined">
                        <DeleteForeverIcon />
                    </Button>
                ) : null}
            </ListItem>
            <Rating
                size="large"
                defaultValue={0}
                precision={1}
                name={name}
                value={parseFloat(score)}
                onChange={onChange(label)}
            />
            {label == 'General' ? <Divider /> : null}
        </div>
    )
}

function MealTypes({ currentMealTypeId, menu, onClick }) {
    if (menu) {
        const mealTypeComponent = menu.mealTypes.map((mt) => {
            const disabled = currentMealTypeId === mt.idx
            return (
                <List key={mt.idx}>
                    <MealType disabled={disabled} mealTypeId={mt.idx} title={mt.title} onClick={onClick(mt.idx)} />
                </List>
            )
        })
        return mealTypeComponent
    } else {
        return null
    }
}

function MealType({ disabled, title, mealTypeId, onClick }) {
    return (
        <ListItem disabled={disabled} button key={mealTypeId} onClick={onClick}>
            <ListItemText primary={title} />
        </ListItem>
    )
}

function CurrentItems({ mealTypeId, items }) {
    if (items) {
        const currentItems = items[mealTypeId] || []
        const itemComponents = currentItems.map((is) => {
            return (
                <ItemCard
                    key={is.item.idx}
                    id={is.item.idx}
                    title={is.item.title}
                    description={is.item.description}
                    price={is.item.price}
                    score={parseFloat(is.globalScore)}
                    imgURL={getImgUrl(is.item.imageId, 350, 200)}
                />
            )
        })

        return itemComponents
    } else {
        return null
    }
}

Menu.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId, mealTypeId } = query

    return { restaurantId, mealTypeId }
}

Menu.propTypes = {
    restaurantId: PropTypes.string.isRequired,
    mealTypeId: PropTypes.string
}

MealType.propTypes = {
    title: PropTypes.string.isRequired,
    mealTypeId: PropTypes.string,
    onClick: PropTypes.func.isRequired,
    disabled: PropTypes.bool.isRequired
}

MealTypes.propTypes = {
    menu: PropTypes.object,
    onClick: PropTypes.func.isRequired,
    currentMealTypeId: PropTypes.string
}

CurrentItems.propTypes = {
    mealTypeId: PropTypes.string,
    items: PropTypes.object
}

Filters.propTypes = {
    filters: PropTypes.object.isRequired,
    filterTypes: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    onReset: PropTypes.func.isRequired
}

Filter.propTypes = {
    i: PropTypes.number.isRequired,
    label: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    onReset: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    score: PropTypes.number
}
