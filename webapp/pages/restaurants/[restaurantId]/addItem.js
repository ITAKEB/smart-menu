import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import CssBaseline from '@material-ui/core/CssBaseline'
import Grid from '@material-ui/core/Grid'
import BathtubIcon from '@material-ui/icons/Bathtub'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import { useRouter } from 'next/router'
import { makeStyles } from '@material-ui/core/styles'
import { useState, React, useEffect } from 'react'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Select from '@material-ui/core/Select'
import PropTypes from 'prop-types'
import Fab from '@material-ui/core/Fab'
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate'

import Layout from '../../../src/components/Layout.js'
import { TextFieldError } from '../../../src/components/InputError'
import { getMenuWithItems, mkItemData, postItem, postItemImage } from '../../../lib/services/restaurantService.js'
import { getUserSession } from '../../../lib/session'
import { registerItemValidator, satisfiesAllRegisterItemValidation } from '../../../lib/validator.js'

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.warning.light
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120
    },
    selectEmpty: {
        marginTop: theme.spacing(2)
    },
    input: {
        display: 'none'
    }
}))

export default function AddItem({ restaurantId }) {
    const router = useRouter()
    const classes = useStyles()
    const [state, setState] = useState({
        tittle: '',
        description: '',
        mealPrice: '',
        file: null,
        menu: null,
        mealTypeId: 0,
        validations: null,
        submitDisabled: true,
        isDuplicatedEmail: false
    })

    const handleChange = (event) => {
        const name = event.target.name
        setState({
            ...state,
            [name]: event.target.value
        })
    }

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()

            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: evt.target.value }
                newState.validations = registerItemValidator(newState)

                if (satisfiesAllRegisterItemValidation(newState.validations) && state.file) {
                    newState.submitDisabled = false
                } else {
                    newState.submitDisabled = true
                }

                return newState
            }

            setState(updateState)
        }
    }

    useEffect(() => {
        const fetchMenu = async () => {
            const { menu } = await getMenuWithItems(restaurantId)
            const mealTypeId = menu.mealTypes[0].idx
            setState((prevSt) => ({ ...prevSt, menu, mealTypeId }))
        }

        fetchMenu()
    }, [restaurantId])

    const handleSubmit = async (evt) => {
        evt.preventDefault()
        console.log(state.title + ' , ' + state.description + ' , ' + state.mealPrice + ' , ' + state.mealTypeId)

        if (state.validations && satisfiesAllRegisterItemValidation(state.validations)) {
            try {
                const { jwt, uid } = getUserSession()
                const { title, description, mealPrice, mealTypeId } = state
                const itemData = mkItemData(title, description, parseInt(mealPrice), mealTypeId)
                const item = await postItem(jwt, uid, mealTypeId, itemData)

                await postItemImage(jwt, uid, item.idx, state.file)

                router.push(`/restaurants/${restaurantId}/menu`)
            } catch (error) {
                console.log(error)
            }
        }
    }

    const handleUploadClick = async (event) => {
        let file = event.target.files[0]

        if (!file) {
            return
        }

        const { type, size } = file

        const maxImgSize = 800000 // 800KBs

        if (size > maxImgSize) {
            setState((prevSt) => ({ ...prevSt, showFail: true, error: 'La imagen debe ser menor o igual a 800KBs' }))
        }

        if (type == 'image/jpeg') {
            //await postImage(jwt, uid, 'df68a7d9-de4d-4d6e-b9e0-30e0d44b103e', file)
            let submitDisabled = true
            if (state.validations && satisfiesAllRegisterItemValidation(state.validations)) {
                submitDisabled = false
            }
            setState((prevSt) => ({ ...prevSt, file, submitDisabled }))
        }
    }

    return (
        <Layout>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <BathtubIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Añadir item
                    </Typography>
                    <form className={classes.form} noValidate onSubmit={handleSubmit}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    name="title"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="title"
                                    label="Titulo"
                                    onChange={handleInputChange('title')}
                                />
                            </Grid>
                            <FieldError validations={state.validations} validationType="titleValidation" />
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="description"
                                    label="Descripción"
                                    name="description"
                                    onChange={handleInputChange('description')}
                                />
                            </Grid>
                            <FieldError validations={state.validations} validationType="descriptionValidation" />
                            <Grid item xs={12} sm={7}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="mealPrice"
                                    label="Precio"
                                    onChange={handleInputChange('mealPrice')}
                                />
                            </Grid>
                            <Grid item xs={12} sm={5}>
                                <InputLabel>Categorias</InputLabel>
                                <FormControl className={classes.formControl}>
                                    <Select
                                        native
                                        value={state.mealTypeId}
                                        onChange={handleChange}
                                        inputProps={{
                                            name: 'mealTypeId',
                                            id: 'mealTypeId'
                                        }}
                                    >
                                        <MealTypes menu={state.menu} />
                                    </Select>
                                </FormControl>
                            </Grid>
                            <FieldError validations={state.validations} validationType="priceValidation" />
                        </Grid>
                        <Grid item xs={12}>
                            <input
                                accept="image/jpeg"
                                className={classes.input}
                                id="contained-button-file"
                                multiple
                                type="file"
                                onChange={handleUploadClick}
                            />
                            <label htmlFor="contained-button-file">
                                <Fab component="span" className={classes.button}>
                                    <AddPhotoAlternateIcon />
                                </Fab>
                            </label>
                        </Grid>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            disabled={state.submitDisabled}
                        >
                            Añadir item
                        </Button>
                    </form>
                </div>
            </Container>
        </Layout>
    )
}

function FieldError({ validations, validationType }) {
    const fieldError =
        !validations || validations[validationType].isValid ? null : (
            <TextFieldError message={validations[validationType].message} />
        )
    return fieldError
}

function MealTypes({ menu }) {
    if (menu) {
        const mealTypeComponent = menu.mealTypes.map((mt) => {
            return <MealType key={mt.idx} mealTypeId={mt.idx} title={mt.title} />
        })
        return mealTypeComponent
    } else {
        return null
    }
}

function MealType({ title, mealTypeId }) {
    return (
        <option key={mealTypeId} value={mealTypeId}>
            {title}
        </option>
    )
}

AddItem.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId } = query

    return { restaurantId }
}

AddItem.propTypes = {
    restaurantId: PropTypes.string.isRequired
}

MealType.propTypes = {
    title: PropTypes.string,
    mealTypeId: PropTypes.string
}

MealTypes.propTypes = {
    menu: PropTypes.object
}
