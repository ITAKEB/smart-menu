import {
    Button,
    Card,
    CardActionArea,
    CardActions,
    CardContent,
    CardMedia,
    Divider,
    Grid,
    Typography
} from '@material-ui/core'
import Link from 'next/link'
import Rating from '@material-ui/lab/Rating'
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'

import { getUserRecommentations } from '../lib/services/userService'
import { getUserSession } from '../lib/session'
import { getImgUrl } from '../lib/services/imageService'

const useStyles = makeStyles({
    root: {
        width: '100%'
    },
    item: {
        marginBottom: '10px',
        marginTop: '10px'
    }
})

export default function Recommendations() {
    const classes = useStyles()
    const [state, setState] = useState({
        recommendItems: null,
        category: 'Quality',
        currentSelected: 'Quality',
        jwt: null,
        uid: null
    })

    useEffect(() => {
        async function fetchData() {
            const { jwt, uid } = getUserSession()

            const recommendItems = await getUserRecommentations(jwt, uid, state.currentSelected, 0, 50)
            setState((prevSt) => ({ ...prevSt, jwt, uid, recommendItems }))
        }

        fetchData()
    }, [state.currentSelected])

    const onClickCat = (category) => {
        return async (evt) => {
            evt.preventDefault()
            const historyItems = await getUserRecommentations(state.jwt, state.uid, category, 0, 50)
            setState((prevSt) => ({ ...prevSt, historyItems, category, currentSelected: category }))
        }
    }

    const categories = [
        {
            onClick: onClickCat('Quality'),
            title: 'Calidad',
            disabled: state.currentSelected === 'Quality'
        },
        {
            onClick: onClickCat('Healthiness'),
            title: 'Salud',
            disabled: state.currentSelected === 'Healthiness'
        },
        {
            onClick: onClickCat('PriceFairness'),
            title: 'Economía',
            disabled: state.currentSelected === 'PriceFairness'
        },
        {
            onClick: onClickCat('Ecology'),
            title: 'Ecología',
            disabled: state.currentSelected === 'Ecology'
        }
    ]

    return (
        <div className={classes.root}>
            <Typography component="h5" variant="h5" align="center" gutterBottom>
                Recomendaciones
            </Typography>
            <Grid container item justifyContent="flex-start" spacing={1} xs={12}>
                {categories.map(({ onClick, title, disabled, avg }) => (
                    <CategoryButton key={title} title={title} onClick={onClick} disabled={disabled} classes={classes} />
                ))}
            </Grid>
            <ItemList items={state.recommendItems} classes={classes} />
        </div>
    )
}

function CategoryButton({ classes, onClick, disabled, title }) {
    return (
        <Grid item xs="auto">
            <Button onClick={onClick} size="small" variant="outlined" color="default" disabled={disabled}>
                {title}
            </Button>
        </Grid>
    )
}

function ItemList({ classes, items }) {
    if (items) {
        return items.map((item, i) => {
            return (
                <div key={i}>
                    <Item
                        title={item.itemTitle}
                        mealTypeId={item.mealTypeId}
                        price={item.price}
                        restaurantId={item.resturantId}
                        score={item.score}
                        imgURL={getImgUrl(item.imageId, 350, 200)}
                        classes={classes}
                    />
                    {items.length == 1 ? null : <Divider />}
                </div>
            )
        })
    } else {
        return null
    }
}

function Item({ title, price, score, imgURL, classes, restaurantId, mealTypeId }) {
    const viewMenuButton = (
        <Link
            href={{
                pathname: '/restaurants/[restaurantId]/menu',
                query: { mealTypeId: mealTypeId }
            }}
            as={`/restaurants/${restaurantId}/menu`}
            passHref
        >
            <Button size="small" color="primary" variant="outlined">
                Ver en menu
            </Button>
        </Link>
    )

    return (
        <Card className={classes.item}>
            <CardActionArea>
                <Grid container spacing={3} alignItems="center" justifyContent="center">
                    <Grid item xs={12} sm={4}>
                        <CardMedia
                            component="img"
                            alt="restaurant item"
                            height="170"
                            image={imgURL}
                            title="restaurant item"
                        />
                    </Grid>
                    <Grid item xs={12} sm={8}>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {title}
                            </Typography>
                            <Rating
                                size="large"
                                name="score"
                                defaultValue={2.5}
                                precision={0.5}
                                value={parseInt(score.toFixed(1))}
                                readOnly
                            />
                            <Typography gutterBottom variant="h5" component="h3">
                                {`$${price}`}
                            </Typography>
                        </CardContent>
                    </Grid>
                </Grid>
            </CardActionArea>
            <CardActions>{viewMenuButton}</CardActions>
        </Card>
    )
}

CategoryButton.propTypes = {
    classes: PropTypes.object.isRequired,
    onClick: PropTypes.func.isRequired,
    disabled: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired
}

ItemList.propTypes = {
    classes: PropTypes.object.isRequired,
    items: PropTypes.array
}

Item.propTypes = {
    title: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    mealTypeId: PropTypes.string.isRequired,
    restaurantId: PropTypes.string.isRequired,
    score: PropTypes.number.isRequired,
    imgURL: PropTypes.string.isRequired,
    classes: PropTypes.object.isRequired
}
