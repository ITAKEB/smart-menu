import { alpha, makeStyles } from '@material-ui/core/styles'
import InputBase from '@material-ui/core/InputBase'
import SearchIcon from '@material-ui/icons/Search'
import { useState, React } from 'react'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { Button, CardActions, CardHeader, Container, Divider, Grid, IconButton } from '@material-ui/core'
import { useRouter } from 'next/router'

import Layout from '../src/components/Layout'
import Logo from '../src/icon/SvgLogo'
import { getHasMenu, getRestaurantByWord } from '../lib/services/restaurantService'
import { getUserSession } from '../lib/session'
import { getImgUrl } from '../lib/services/imageService'

const useStyles = makeStyles((theme) => ({
    search: {
        display: 'flex',
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        border: '1px solid black',
        backgroundColor: alpha(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: alpha(theme.palette.common.white, 0.25)
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%'
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        boxSizing: 'border-box',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        cursor: 'pointer',
        zIndex: 2
    },
    inputRoot: {
        color: 'inherit',
        width: '100%'
    },
    icon: {
        cursor: 'pointer'
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(5)}px)`,
        width: '100%',
        zIndex: 1
    },
    form: {
        width: '100%',
        backgroundColor: 'black'
    },
    root: {
        maxWidth: '100%',
        marginTop: '20px',
        cursor: 'pointer'
    },
    media: {
        height: 0,
        paddingTop: '56.25%' // 16:9
    },
    svgContainer: {
        display: 'flex',
        alignContent: 'center',
        justifyContent: 'center'
    }
}))

export default function Home() {
    const classes = useStyles()
    const [state, setState] = useState({
        search: '',
        restaurants: null
    })

    const handleOnClick = async (evt) => {
        evt.preventDefault()

        const { restaurants } = await getRestaurantByWord(state.search)
        setState((prevSt) => ({ ...prevSt, restaurants }))
    }

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()
            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: evt.target.value }

                return newState
            }

            setState(updateState)
        }
    }

    return (
        <Layout>
            <Container maxWidth="md">
                <br />
                <div className={classes.svgContainer}>
                    <Logo />
                </div>
                <form className={classes.search} onSubmit={handleOnClick}>
                    <div className={classes.searchIcon} onClick={handleOnClick}>
                        <SearchIcon />
                    </div>

                    <InputBase
                        placeholder="Search…"
                        classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput
                        }}
                        inputProps={{ 'aria-label': 'search' }}
                        onChange={handleInputChange('search')}
                    />
                </form>
                <div>
                    <Grid container spacing={3} justifyContent="center">
                        {state.restaurants ? <Restaurants restaurants={state.restaurants} classes={classes} /> : null}
                    </Grid>
                </div>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </Container>
        </Layout>
    )
}

function Restaurants({ restaurants, classes }) {
    if (restaurants.length < 1) {
        return (
            <Grid item container xs={12} justifyContent="center">
                <Typography variant="body2" color="error" component="p">
                    No se encontró ninguna coincidencia
                </Typography>
            </Grid>
        )
    } else {
        return restaurants.map((r) => {
            return (
                <Grid key={r.idx} item xs={12} sm={4}>
                    <RestaurantOption
                        logo={r.imageId}
                        nombre={r.name}
                        email={r.email}
                        country={r.country}
                        city={r.city}
                        description={r.description}
                        phoneNumber={r.phoneNumber}
                        address={r.address}
                        classes={classes}
                        restaurantId={r.idx}
                    />
                </Grid>
            )
        })
    }
}

function RestaurantOption(props) {
    const { nombre, logo, email, country, city, address, phoneNumber, description, restaurantId, classes } = props
    const router = useRouter()

    const handleOnClick = async (evt) => {
        evt.preventDefault()
        const hasMenu = await getHasMenu(restaurantId)
        if (hasMenu) {
            router.push(`/restaurants/${restaurantId}/menu`)
        } else {
            router.push(`/restaurants/${restaurantId}`)
        }
    }
    return (
        <Card className={classes.root}>
            <CardActionArea onClick={handleOnClick}>
                <CardMedia className={classes.media} image={getImgUrl(logo, 400, 300)} title="Paella dish" />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {nombre}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {email}
                        <br />
                        {country}
                        <br />
                        {city}
                        <br />
                        {address}
                        <br />
                        {phoneNumber}
                        <br />
                        {description}
                    </Typography>
                </CardContent>

                <CardActions disableSpacing></CardActions>
            </CardActionArea>
        </Card>
    )
}

Restaurants.propTypes = {
    restaurants: PropTypes.array,
    classes: PropTypes.object
}

RestaurantOption.propTypes = {
    nombre: PropTypes.string.isRequired,
    logo: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired,
    city: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    phoneNumber: PropTypes.string.isRequired,
    description: PropTypes.string,
    restaurantId: PropTypes.string.isRequired,
    classes: PropTypes.object.isRequired,
    onClick: PropTypes.func
}
