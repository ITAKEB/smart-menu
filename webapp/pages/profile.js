import React, { useState, useEffect } from 'react'
import clsx from 'clsx'
import { makeStyles, useTheme, createTheme } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import List from '@material-ui/core/List'
import CssBaseline from '@material-ui/core/CssBaseline'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import IconButton from '@material-ui/core/IconButton'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import AccountBoxIcon from '@material-ui/icons/AccountBox'
import RestaurantMenuIcon from '@material-ui/icons/RestaurantMenu'
import HistoryIcon from '@material-ui/icons/History'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import MenuBookIcon from '@material-ui/icons/MenuBook'
import Rating from '@material-ui/lab/Rating'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import { useRouter } from 'next/router'
import Grid from '@material-ui/core/Grid'

import PropTypes from 'prop-types'

import Layout from '../src/components/Layout'
import Recomendations from './recommendations'
import { Button, Container } from '@material-ui/core'
import { getHasMenu, getRestaurants } from '../lib/services/restaurantService'
import { clearUserSession, getUserSession } from '../lib/session'
import RestaurantCard from '../src/components/RestaurantCard'
import { getImgUrl } from '../lib/services/imageService'
import { getUserHistory, getUserPerformance, revokeToken } from '../lib/services/userService'
import { InfoOutlined } from '@material-ui/icons'
import Popup from '../src/components/Popup'
import RestaurantScores from './restaurantScores'

const drawerWidth = 240

const theme = createTheme({})

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        width: '100%'
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        marginTop: '70px',
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    menuButton: {
        marginRight: 36
    },
    hide: {
        display: 'none'
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap'
    },
    drawerOpen: {
        position: 'static',
        marginTop: '5px',
        overflow: 'hidden',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    drawerClose: {
        position: 'static',
        marginTop: '6px',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        }),
        overflow: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
            marginTop: '2px'
        }
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3)
    },
    link: {
        margin: theme.spacing(1, 1.5)
    },
    cardRoot: {
        maxWidth: '100%',
        marginTop: '2%'
    },
    media: {
        height: '200px'
    },
    itemsRoot: {
        width: '100%'
    },
    item: {
        marginBottom: '10px',
        marginTop: '10px'
    },
    rootScores: {
        marginTop: 1,
        width: '100%'
    },
    prueba: {
        backgroundColor: 'red'
    },
    likes: {
        display: 'flex',
        alignItems: 'center',
        alignContent: 'center'
    },
    header: {
        display: 'flex',
        justifyContent: 'space-between'
    }
}))

export default function Profile() {
    const classes = useStyles(theme)

    const [state, setState] = useState({
        open: true,
        currentSelected: 'Perfil',
        email: '',
        isAdmin: false,
        restaurants: null
    })

    useEffect(() => {
        const fetchRestaurant = async () => {
            const { email, isAdmin, jwt, uid } = getUserSession()

            if (isAdmin) {
                const restaurants = await getRestaurants(jwt, uid)

                setState((prevSt) => ({ ...prevSt, email, isAdmin, restaurants }))
            } else {
                setState((prevSt) => ({ ...prevSt, email }))
            }
        }

        fetchRestaurant()
    }, [])

    const handleDrawerOpen = () => {
        setState((prevSt) => ({ ...prevSt, open: true }))
    }

    const handleDrawerClose = () => {
        setState((prevSt) => ({ ...prevSt, open: false }))
    }

    const onClickOp = (option) => {
        return async (evt) => {
            evt.preventDefault()

            setState((prevSt) => ({ ...prevSt, option, currentSelected: option }))
        }
    }

    const options = [
        {
            onClick: onClickOp('Perfil'),
            title: 'Perfil',
            disabled: state.currentSelected === 'Perfil'
        },
        {
            onClick: onClickOp('Historial'),
            title: 'Historial',
            disabled: state.currentSelected === 'Historial'
        },
        {
            onClick: onClickOp('Mis restaurantes'),
            title: 'Mis restaurantes',
            disabled: state.currentSelected === 'Mis restaurantes'
        },
        {
            onClick: onClickOp('Recomendaciones'),
            title: 'Recomendaciones',
            disabled: state.currentSelected === 'Recomendaciones'
        }
    ]

    return (
        <Layout>
            <div className={classes.root}>
                <CssBaseline />
                <AppBar
                    color="primary"
                    position="absolute"
                    className={clsx(classes.appBar, {
                        [classes.appBarShift]: state.open
                    })}
                >
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={handleDrawerOpen}
                            edge="start"
                            className={clsx(classes.menuButton, {
                                [classes.hide]: state.open
                            })}
                        >
                            <ChevronRightIcon />
                        </IconButton>
                        <Typography variant="h6" noWrap>
                            {state.currentSelected}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Drawer
                    variant="permanent"
                    className={clsx(classes.drawer, {
                        [classes.drawerOpen]: state.open,
                        [classes.drawerClose]: !state.open
                    })}
                    classes={{
                        paper: clsx({
                            [classes.drawerOpen]: state.open,
                            [classes.drawerClose]: !state.open
                        })
                    }}
                >
                    <div className={classes.toolbar}>
                        <p>{state.email}</p>
                        <IconButton onClick={handleDrawerClose}>
                            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                        </IconButton>
                    </div>
                    <Divider />
                    <List>
                        {options.map(({ onClick, title, disabled }, index) => (
                            <OptionButton
                                key={title}
                                index={index}
                                title={title}
                                onClick={onClick}
                                disabled={disabled}
                                isAdmin={state.isAdmin}
                            />
                        ))}
                    </List>
                </Drawer>
                <main className={classes.content}>
                    <div className={classes.toolbar} />
                    <CurrentOption
                        currentSelected={state.currentSelected}
                        email={state.email}
                        restaurants={state.restaurants}
                        classes={classes}
                    />
                </main>
            </div>
        </Layout>
    )
}

function CurrentOption({ currentSelected, email, restaurants, classes }) {
    let current = null
    if (currentSelected == 'Mis restaurantes') {
        current = <RestaurantLinks classes={classes} restaurants={restaurants} />
    }
    if (currentSelected == 'Perfil') {
        current = <ProfileInfo email={email} classes={classes} />
    }
    if (currentSelected == 'Historial') {
        current = <History classes={classes} />
    }
    if (currentSelected == 'Recomendaciones') {
        current = <Recomendations classes={classes} />
    }
    return (
        <Grid container justifyContent="center">
            {current}
        </Grid>
    )
}

function OptionButton({ onClick, disabled, title, index, isAdmin }) {
    let iconButton = undefined
    if (title == 'Mis restaurantes' && !isAdmin) {
        return null
    }
    if (title == 'Perfil') {
        iconButton = <AccountBoxIcon />
    }
    if (title == 'Mis restaurantes') {
        iconButton = <RestaurantMenuIcon />
    }
    if (title == 'Historial') {
        iconButton = <HistoryIcon />
    }
    if (title == 'Recomendaciones') {
        iconButton = <MenuBookIcon />
    }
    return (
        <ListItem button onClick={onClick} disabled={disabled} key={title}>
            <ListItemIcon>{iconButton}</ListItemIcon>
            <ListItemText primary={title} />
        </ListItem>
    )
}

function History({ classes }) {
    const [state, setState] = useState({
        items: '',
        category: 'Quality',
        currentSelected: 'Quality',
        historyItems: null,
        jwt: null,
        uid: null
    })

    useEffect(() => {
        async function fetchData() {
            const { jwt, uid } = getUserSession()

            const historyItems = await getUserHistory(jwt, uid, state.currentSelected, 0, 50)

            setState((prevSt) => ({ ...prevSt, jwt, uid, historyItems }))
        }

        fetchData()
    }, [state.currentSelected])

    const onClickCat = (category) => {
        return async (evt) => {
            evt.preventDefault()
            const historyItems = await getUserHistory(state.jwt, state.uid, category, 0, 50)
            setState((prevSt) => ({ ...prevSt, historyItems, category, currentSelected: category }))
        }
    }

    const categories = [
        {
            onClick: onClickCat('Quality'),
            title: 'Calidad',
            disabled: state.currentSelected === 'Quality'
        },
        {
            onClick: onClickCat('Healthiness'),
            title: 'Salud',
            disabled: state.currentSelected === 'Healthiness'
        },
        {
            onClick: onClickCat('PriceFairness'),
            title: 'Economía',
            disabled: state.currentSelected === 'PriceFairness'
        },
        {
            onClick: onClickCat('Ecology'),
            title: 'Ecología',
            disabled: state.currentSelected === 'Ecology'
        }
    ]

    const component = (
        <div className={classes.itemsRoot}>
            <Typography component="h5" variant="h5" align="center" gutterBottom>
                Historial de reviews
            </Typography>
            <Grid container item justifyContent="flex-start" spacing={1} xs={12}>
                {categories.map(({ onClick, title, disabled, avg }) => (
                    <CategoryButton key={title} title={title} onClick={onClick} disabled={disabled} classes={classes} />
                ))}
            </Grid>

            <ItemList classes={classes} items={state.historyItems} />
        </div>
    )

    return component
}
function CategoryButton({ classes, onClick, disabled, title }) {
    return (
        <Grid item xs="auto">
            <Button onClick={onClick} size="small" variant="outlined" color="default" disabled={disabled}>
                {title}
            </Button>
        </Grid>
    )
}

function ItemList({ classes, items }) {
    if (items) {
        const component = items.map((item, k) => {
            return (
                <div key={k}>
                    <Grid item xs={12}>
                        <Item
                            classes={classes}
                            imgURL={getImgUrl(item.imageId, 350, 200)}
                            title={item.itemTitle}
                            score={item.score}
                            price={item.price}
                            description={item.content}
                        />
                    </Grid>
                    <Divider />
                </div>
            )
        })

        return items ? component : null
    } else {
        return null
    }
}

function Item({ classes, title, score, price, description, imgURL }) {
    return (
        <Card className={classes.item}>
            <CardActionArea>
                <Grid container spacing={3} alignItems="center" justifyContent="center">
                    <Grid item xs={12} sm={4}>
                        <CardMedia
                            component="img"
                            alt="restaurant item"
                            height="170"
                            image={imgURL}
                            title="restaurant item"
                        />
                    </Grid>
                    <Grid item xs={12} sm={8}>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {title}
                            </Typography>
                            <Typography gutterBottom variant="h5" component="h3">
                                {`$${price}`}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                {description}
                            </Typography>
                            <Rating
                                size="large"
                                name="score"
                                defaultValue={2.5}
                                precision={0.5}
                                value={parseFloat(score.toFixed(1))}
                                readOnly
                            />
                        </CardContent>
                    </Grid>
                </Grid>
            </CardActionArea>
        </Card>
    )
}

function ProfileInfo({ name, email, classes }) {
    const router = useRouter()
    const [state, setState] = useState({
        scores: {
            Quality: { value: 0 },
            Healthiness: { value: 0 },
            PriceFairness: { value: 0 },
            Ecology: { value: 0 }
        }
    })

    const scoreTypes = ['Quality', 'Healthiness', 'PriceFairness', 'Ecology']

    const handleCloseSesion = async () => {
        try {
            const { uid, jwt } = getUserSession()

            clearUserSession()
            await revokeToken(jwt, uid)
            router.push('/signin')
        } catch (error) {
            console.log(error)
        }
    }

    const component = (
        <div className={classes.rootScores}>
            <Typography component="h5" variant="h5" align="center" gutterBottom>
                Nombre
            </Typography>
            <Grid container justifyContent="center" alignItems="center">
                {email}
            </Grid>
            <Grid item container justifyContent="center" alignItems="center">
                <Button onClick={handleCloseSesion} color="primary" variant="outlined" className={classes.link}>
                    Cerrar sesión
                </Button>
            </Grid>
            <Card>
                {scoreTypes.map((m, i) => {
                    return <UserScores key={i} classes={classes} title={m} />
                })}
            </Card>
        </div>
    )

    return component
}

const scoreTypes = ['Quality', 'Healthiness', 'PriceFairness', 'Ecology']

function toTitle(category) {
    if (category == 'Quality') {
        return 'Calidad'
    }

    if (category == 'Healthiness') {
        return 'Salud'
    }

    if (category == 'PriceFairness') {
        return 'Economía'
    }

    if (category == 'Ecology') {
        return 'Ecología'
    }

    return category
}

function UserScores({ title, classes }) {
    const [state, setState] = useState({
        score: 0
    })

    useEffect(() => {
        async function fetchData() {
            const { jwt, uid } = getUserSession()

            const res = await getUserPerformance(jwt, uid, title)
            const score = res.score
            setState((prevSt) => ({ ...prevSt, score }))
        }

        fetchData()
    }, [title])

    return (
        <CardActions className={classes.rootScores}>
            <Grid container spacing={3} alignItems="center" justifyContent="center">
                <Grid item xs={12}>
                    <CardContent>
                        <Typography className={classes.header} variant="h6">
                            {toTitle(title)}
                        </Typography>

                        <Rating
                            size="medium"
                            readOnly
                            name="rating"
                            defaultValue={2.5}
                            precision={0.1}
                            value={state.score}
                        />
                    </CardContent>
                </Grid>
            </Grid>
        </CardActions>
    )
}

function RestaurantLinks({ classes, restaurants }) {
    if (restaurants) {
        const links = restaurants.map((r) => <RestaurantLink key={r.idx} classes={classes} restaurant={r} />)
        const component = (
            <Grid container justifyContent="center">
                <Button href="/restaurants/register" color="primary" variant="outlined">
                    Crear Restaurante
                </Button>
                <Grid container spacing={3} justifyContent="center">
                    {links}
                </Grid>
            </Grid>
        )

        return restaurants ? component : null
    } else {
        return null
    }
}

function RestaurantLink({ classes, restaurant }) {
    const [state, setState] = useState({
        hasMenu: false,
        openPopup: false
    })

    useEffect(() => {
        async function fetchData() {
            const hasMenu = await getHasMenu(restaurant.idx)
            setState((prevSt) => ({ ...prevSt, hasMenu }))
        }

        fetchData()
    }, [restaurant.idx])

    const handleClosedRating = () => {
        setState((prevSt) => ({ ...prevSt, openPopup: false }))
    }

    return (
        <Grid item xs={12} sm={4}>
            <RestaurantCard
                classes={classes}
                restaurantId={restaurant.idx}
                isAdmin={true}
                hasMenu={state.hasMenu}
                nombre={restaurant.name}
                logo={getImgUrl(restaurant.imageId, 500, 500)}
                email={restaurant.email}
                country={restaurant.country}
                city={restaurant.city}
                address={restaurant.address}
                phoneNumber={restaurant.phoneNumber}
                onClick={() => {
                    setState((prevSt) => ({ ...prevSt, openPopup: true }))
                }}
            />
            <Popup
                title={`Promedio de ${restaurant.name}`}
                openPopup={state.openPopup}
                setOpenPopup={handleClosedRating}
            >
                <RestaurantScores restaurantId={restaurant.idx} />
            </Popup>
        </Grid>
    )
}

CurrentOption.propTypes = {
    currentSelected: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    restaurants: PropTypes.array,
    classes: PropTypes.object.isRequired
}

OptionButton.propTypes = {
    onClick: PropTypes.func.isRequired,
    disabled: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
    isAdmin: PropTypes.bool.isRequired
}

History.propTypes = {
    classes: PropTypes.object.isRequired
}

CategoryButton.propTypes = {
    classes: PropTypes.object.isRequired,
    onClick: PropTypes.func.isRequired,
    disabled: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired
}

Item.propTypes = {
    classes: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
    price: PropTypes.number.isRequired,
    score: PropTypes.number.isRequired,
    imgURL: PropTypes.string.isRequired
}

ProfileInfo.propTypes = {
    classes: PropTypes.object.isRequired,
    email: PropTypes.string.isRequired,
    name: PropTypes.string
}

UserScores.propTypes = {
    title: PropTypes.string.isRequired,
    classes: PropTypes.object,
    id: PropTypes.string
}

RestaurantLinks.propTypes = {
    classes: PropTypes.object.isRequired,
    restaurants: PropTypes.array.isRequired
}

RestaurantLink.propTypes = {
    classes: PropTypes.object.isRequired,
    restaurant: PropTypes.object.isRequired
}

ItemList.propTypes = {
    classes: PropTypes.object.isRequired,
    items: PropTypes.arrayOf(PropTypes.object)
}
