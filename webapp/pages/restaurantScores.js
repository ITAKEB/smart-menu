import React, { useState, useEffect } from 'react'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Rating from '@material-ui/lab/Rating'
import { makeStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { getMenuWithItems } from '../lib/services/restaurantService'
import { TimeToLeaveRounded } from '@material-ui/icons'
import { Container } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: 1
    },
    prueba: {
        backgroundColor: 'red'
    },
    likes: {
        display: 'flex',
        alignItems: 'center',
        alignContent: 'center'
    },
    header: {
        display: 'flex',
        justifyContent: 'space-between'
    }
}))

export default function RestaurantScores({ restaurantId }) {
    const classes = useStyles()

    const [state, setState] = useState({
        hasMenu: false,
        penPopup: false,
        items: null,
        menu: null
    })

    useEffect(() => {
        async function fetchData() {
            const { menu, items } = await getMenuWithItems(restaurantId)

            avgScores(menu, items)
            setState((prevSt) => ({ ...prevSt, menu, items }))
        }

        fetchData()
    }, [restaurantId])

    const avgScores = (menu, items) => {
        menu.mealTypes.map((m) => {
            let prom = 0

            items[m.idx].map((item, i) => {
                prom += item.globalScore
            })

            m.avgScore = prom / items[m.idx].length
        })
    }

    return (
        <Card>
            {state.menu
                ? state.menu.mealTypes.map((m) => {
                      return (
                          <MealTypeInfo key={m.idx} id={m.idx} classes={classes} title={m.title} score={m.avgScore} />
                      )
                  })
                : null}
        </Card>
    )
}

function MealTypeInfo({ title, score, classes, id }) {
    return (
        <CardActions className={classes.root} key={id}>
            <Grid container spacing={3} alignItems="center" justifyContent="center">
                <Grid item xs={12}>
                    <CardContent>
                        <Typography className={classes.header} variant="h6">
                            {title}
                        </Typography>

                        <Rating
                            size="medium"
                            readOnly
                            name="rating"
                            defaultValue={2.5}
                            precision={0.5}
                            value={parseFloat(score)}
                        />
                    </CardContent>
                </Grid>
            </Grid>
        </CardActions>
    )
}

RestaurantScores.propTypes = {
    restaurantId: PropTypes.string.isRequired
}

MealTypeInfo.propTypes = {
    title: PropTypes.string.isRequired,
    score: PropTypes.number.isRequired,
    classes: PropTypes.object,
    id: PropTypes.string
}
