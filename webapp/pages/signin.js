import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import CssBaseline from '@material-ui/core/CssBaseline'
import Grid from '@material-ui/core/Grid'
import Link from '@material-ui/core/Link'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import { useState, React } from 'react'
import { useRouter } from 'next/router'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'

import Layout from '../src/components/Layout'
import { basicAuth } from '../lib/services/userService'
import { TextFieldError } from '../src/components/InputError'
import { jwtToJSON, mkSession, storeUserSession } from '../lib/session'

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    }
}))

export default function SignIn() {
    const classes = useStyles()
    const [state, setState] = useState({ email: '', password: '', isUnknownUser: false })
    const router = useRouter()

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()
            setState((prevSt) => ({ ...prevSt, [attr]: evt.target.value, isUnknownUser: false }))
        }
    }

    const handleSubmit = async (evt) => {
        evt.preventDefault()
        try {
            const { user, token } = await basicAuth(state.email.trim(), state.password)
            const payload = jwtToJSON(token.jwt)
            const session = mkSession(payload.sub, payload.aud, token.jwt, user.isAdmin)

            storeUserSession(session)
            router.push('/profile')
        } catch (error) {
            console.log(error)
            if (error === 'Unauthorized') {
                setState((prevSt) => ({ ...prevSt, isUnknownUser: true }))
            } else {
                console.log(error)
            }
        }
    }

    return (
        <Layout>
            <Container component="main" maxWidth="xs" onSubmit={handleSubmit}>
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Iniciar Sesión
                    </Typography>
                    <form className={classes.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Correo Electrónico"
                            name="email"
                            autoComplete="email"
                            autoFocus
                            onChange={handleInputChange('email')}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Contraseña"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={handleInputChange('password')}
                        />
                        {state.isUnknownUser ? <TextFieldError message="Invalid email or password." /> : null}
                        <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
                            Iniciar Sesión
                        </Button>
                        <Grid container justifyContent="center">
                            <Grid item>
                                <Link href="/signup" variant="body2">
                                    {'¿No tienes una cuenta? Registrate'}
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
        </Layout>
    )
}
