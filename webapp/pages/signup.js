import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import Checkbox from '@material-ui/core/Checkbox'
import Container from '@material-ui/core/Container'
import CssBaseline from '@material-ui/core/CssBaseline'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Grid from '@material-ui/core/Grid'
import Link from '@material-ui/core/Link'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import { useRouter } from 'next/router'
import { useState, React } from 'react'

import Layout from '../src/components/Layout.js'
import { satisfiesAllSignUpValidations, signUpValidator } from '../lib/validator.js'
import { TextFieldError } from '../src/components/InputError'
import { postUser, mkUser } from '../lib/services/userService.js'
import { jwtToJSON, mkSession, storeUserSession } from '../lib/session.js'

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200
    }
}))

export default function SignUp() {
    const router = useRouter()
    const classes = useStyles()
    const [state, setState] = useState({
        name: '',
        email: '',
        password: '',
        confirmPassword: '',
        confirmationCode: '',
        country: '',
        city: '',
        isAdmin: false,
        birthdate: '2000-01-01',
        validations: null,
        submitDisabled: true,
        isDuplicatedEmail: false
    })

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()
            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: evt.target.value, isDuplicatedEmail: false }
                newState.validations = signUpValidator(newState)

                if (satisfiesAllSignUpValidations(newState.validations)) {
                    newState.submitDisabled = false
                } else {
                    newState.submitDisabled = true
                }

                return newState
            }

            setState(updateState)
        }
    }

    const handleSubmit = async (evt) => {
        evt.preventDefault()

        if (state.validations && satisfiesAllSignUpValidations(state.validations)) {
            try {
                const userData = mkUser(
                    state.email,
                    state.name,
                    state.password,
                    state.city,
                    state.country,
                    state.birthdate,
                    state.isAdmin
                )
                const { jwt } = await postUser(userData)
                const payload = jwtToJSON(jwt)
                const session = mkSession(payload.sub, payload.aud, jwt, state.isAdmin)

                storeUserSession(session)

                router.push('/profile')
            } catch (error) {
                console.log(error)

                if (error === 'ExistentEmail') {
                    setState((prevSt) => ({ ...prevSt, isDuplicatedEmail: true }))
                }
            }
        } else {
            console.log('Sing up attempt with invalid form fields.')
        }
    }

    const handleCheckboxChange = async (evt) => {
        evt.preventDefault()
        setState((prevSt) => ({ ...prevSt, isAdmin: !prevSt.isAdmin }))
    }

    const duplicatedEmailError = state.isDuplicatedEmail ? <TextFieldError message="Email is already taken" /> : null

    return (
        <Layout>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Registrarte
                    </Typography>
                    <form className={classes.form} noValidate onSubmit={handleSubmit}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    name="name"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="name"
                                    label="Nombre"
                                    onChange={handleInputChange('name')}
                                />
                                <FieldError validations={state.validations} validationType="nameValidation" />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Correo Electrónico"
                                    name="email"
                                    onChange={handleInputChange('email')}
                                />
                                <FieldError validations={state.validations} validationType="emailValidation" />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    name="country"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="country"
                                    label="País"
                                    onChange={handleInputChange('country')}
                                />
                                <FieldError validations={state.validations} validationType="countryValidation" />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    name="city"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="city"
                                    label="Ciudad"
                                    onChange={handleInputChange('city')}
                                />
                                <FieldError validations={state.validations} validationType="cityValidation" />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    id="date"
                                    label="Fecha de nacimiento"
                                    type="date"
                                    defaultValue="2000-01-01"
                                    className={classes.textField}
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleInputChange('birthdate')}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Contraseña"
                                    type="password"
                                    id="password"
                                    onChange={handleInputChange('password')}
                                />
                                <FieldError validations={state.validations} validationType="passwordValidation" />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="confirmPassword"
                                    label="Confirmar Contraseña"
                                    type="password"
                                    id="confirmPassword"
                                    onChange={handleInputChange('confirmPassword')}
                                />
                            </Grid>
                            {duplicatedEmailError}
                            <Grid item xs={12}>
                                <FormControlLabel
                                    control={
                                        <Checkbox onChange={handleCheckboxChange} value="isAdmin" color="primary" />
                                    }
                                    label="Soy administrador de restaurante"
                                />
                            </Grid>
                        </Grid>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            disabled={state.submitDisabled}
                        >
                            Registrarte
                        </Button>
                        <Grid container justifyContent="center">
                            <Grid item>
                                <Link href="/signin" variant="body2">
                                    {'¿Ya tienes una cuenta? Inicia sesión'}
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
        </Layout>
    )
}

function FieldError({ validations, validationType }) {
    const fieldError =
        !validations || validations[validationType].isValid ? null : (
            <TextFieldError message={validations[validationType].message} />
        )
    return fieldError
}
