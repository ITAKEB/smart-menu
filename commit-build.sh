#!/bin/bash

set -xeuf -o pipefail

export PATH="$PATH:/root/.local/bin"

make build-webapp

make check-style
CHECK_STATUS_CODE=$?

make run-tests

if [ $CHECK_STATUS_CODE -ne 0 ]; then
   echo 'commit-build has failed'
else
   make package
fi
