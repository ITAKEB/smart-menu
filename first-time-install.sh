#!/bin/bash

set -xeuf -o pipefail

if [[ -v CI ]]; then
   SUDO=""
else
   SUDO="sudo"
fi

# Install system deps
$SUDO apt update
$SUDO apt install -y tar libtinfo-dev libncurses-dev build-essential lsb-release lzip curl

# Install NPM and NODEJS
$SUDO apt update
$SUDO apt install -y curl

curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh

$SUDO bash nodesource_setup.sh
$SUDO apt install -y nodejs

# Install Haskell stack
if ! type "stack" > /dev/null; then
    curl -sSL https://get.haskellstack.org/ | sh
else
    echo 'stack already installed'
fi

# Install Haskell's code formatter
stack install brittany-0.13.1.0

# Install Postgres dev tools
${SUDO} apt-get install -y libpq-dev

# Install Docker
if [[ -v CI ]]; then
    echo "On CI, skipping Docker installation"
else
    ${SUDO} apt install -y apt-transport-https ca-certificates curl software-properties-common

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | ${SUDO} apt-key add -

    ${SUDO} add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"

    ${SUDO} apt update

    ${SUDO} apt-cache policy docker-ce

    ${SUDO} apt install -y docker-ce

    egrep -i "^docker" /etc/group;
    if [ $? -eq 0 ]; then
        echo "Group docker exists"
        ${SUDO} usermod -aG docker $USER
    else
        ${SUDO} groupadd docker
        ${SUDO} usermod -aG docker $USER
    fi

    #Install Docker Compose
    ${SUDO} curl -L https://github.com/docker/compose/releases/download/1.29.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    ${SUDO} chmod +x /usr/local/bin/docker-compose
fi
