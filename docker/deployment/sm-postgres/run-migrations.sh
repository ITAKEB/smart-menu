#!/bin/bash

set -xeuf -o pipefail

/opt/schemas/bin/run-migration -c /opt/schemas/config/config.yaml
