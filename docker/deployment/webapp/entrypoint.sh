#!/bin/bash

set -xeuf -o pipefail

npm run build && npm run start
