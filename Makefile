DIST_DIR:=${CURDIR}/dist
PACKAGE_DIR:=${CURDIR}/_build/packages

COMMIT_SHA:=$(if $(CI_COMMIT_SHA),$(CI_COMMIT_SHA),$(shell git rev-parse --verify HEAD))
SHORT_SHA:=$(shell echo ${COMMIT_SHA} | cut -c1-8)
COMPRESION_LEVEL:=$(if $(CI),9,1)
OS_CODE:=$(shell lsb_release -cs)
BRANCH:=$(if $(CI_COMMIT_BRANCH),$(CI_COMMIT_BRANCH),$(shell git rev-parse --abbrev-ref HEAD))
BUILD_NUMBER:=$(if $(CI_PIPELINE_IID),$(CI_PIPELINE_IID),$(shell git rev-list --count HEAD))
MAJOR_VERSION:=0
MINOR_VERSION:=1
PACKAGE_NAME:=smart-menu-${MAJOR_VERSION}.${MINOR_VERSION}.${BUILD_NUMBER}-${SHORT_SHA}--ubuntu-${OS_CODE}

.PHONY: package
package: build-dist
	mkdir -p ${PACKAGE_DIR}
	$(eval FINGERPRINT:=$(shell python3.8 fingerprint.py ${DIST_DIR}))
	tar chvf ${PACKAGE_NAME}--${FINGERPRINT}.tar ./dist && mv ${CURDIR}/${PACKAGE_NAME}--${FINGERPRINT}.tar ${PACKAGE_DIR}
	lzip -f -${COMPRESION_LEVEL} ${PACKAGE_DIR}/${PACKAGE_NAME}--${FINGERPRINT}.tar

.PHONY: build-dist
build-dist:
	rm -rf ${DIST_DIR}
	mkdir -p ${DIST_DIR}
	cd ./webapi && stack clean && stack install --pedantic --local-bin-path=${DIST_DIR}
	touch ${DIST_DIR}/version
	echo "repository: smart-menu" >> ${DIST_DIR}/version
	echo "branch: ${BRANCH}" >> ${DIST_DIR}/version
	echo "commit: ${COMMIT_SHA}" >> ${DIST_DIR}/version

.PHONY: build-webapp
build-webapp:
	cd webapp && npm install && npm run build

.PHONY: run-tests
run-tests:
	cd ./webapi && stack test --pedantic

.PHONY: check-style
check-style:
	${CURDIR}/webapi/style.sh check
	cd ${CURDIR}/webapp && npm run check-style
	cd ${CURDIR}/webapp && npm run lint

.PHONY: style
style:
	${CURDIR}/webapi/style.sh
	cd ${CURDIR}/webapp && npm run style

.PHONY: clean
clean:
	cd ./webapi && stack clean
	rm -rf ${DIST_DIR}
	rm -rf ${PACKAGE_DIR}
	cd ./webapp && rm -rf node_modules
	cd ./webapp && rm -rf package-lock.json
